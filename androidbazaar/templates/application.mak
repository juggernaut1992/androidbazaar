<%inherit file="local:templates.master"/>

<%
    from khayyam import JalaliDatetime
    latest_release = application.latest_release()
    logo = [t for t in application.thumbnails if t.variant == 'logo']
    logo_key = logo[0].key if logo else ''
    logo_filename = logo[0].filename if logo else ''

    def normalize_permissions(permissions):
        return permissions.replace('}', '').replace('{', '').split(',')

    def find_logo(application):
        logo = [t for t in application.thumbnails if t.variant == 'logo']
        logo_key = logo[0].key if logo else ''
        logo_filename = logo[0].filename if logo else ''
        return tg.url('/storage/{}/{}'.format(logo_key, logo_filename))

    def space_to_dash(s):
        return s.replace(' ', '-')
%>



<div class="container">

    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="box_single_product">
                <div class="col-md-2">
                    <img style="max-width: 115%" class="thumbnail img-responsive img-circle" src="${tg.url('/storage/{}/{}'.format(logo_key, logo_filename)) if logo_key else ''}" alt="">
                </div>
                <div class="col-md-10">
                    <div class="title_product">
                        ${application.label}
                    </div>

                    <div class="platform">
                        <span style="color: green"><a href="${tg.url('/developer/{}/{}'.format(application.tg_user.uid, application.tg_user.user_name))}">${application.tg_user.user_name}</a></span>
                    </div>
                    <div class="star">
                        <span class="star_point">
                            <span class="star-rating">
                                <span class="fa fa-star-o" data-rating="1"></span>
                                <span class="fa fa-star-o" data-rating="2"></span>
                                <span class="fa fa-star-o" data-rating="3"></span>
                                <span class="fa fa-star-o" data-rating="4"></span>
                                <span class="fa fa-star-o" data-rating="5"></span>
                                <input type="hidden" name="whatever1" class="rating-value" value="1">
                            </span>
                        </span>
                        ۴.۶\۵ ( ${len(application.comments)} نظر )
                    </div>
                    <div class="btn btn-info dwn">
                        دریافت
                    </div>
                    <div class="social_single">
##                         <ul class="social_footer">
##                             %for t in [t for t in application.thumbnails if t.variant == 'thumbnail']:
##                                 <li><a href=""><img src="${tg.url('/storage/{}/{}'.format(t.key, t.filename))}" alt=""></a></li>
##                             %endfor
##                         </ul>
                    </div>
                </div>

                <div class="body_single_product">
                    <ul class="box_products" style="display: -webkit-inline-box; direction: rtl;">

                    % for t in [t for t in application.thumbnails if t.variant == 'thumbnail']:


                                            <li style="margin: 10px; direction: rtl;">
                                                <a target="_blank" href="${tg.url('/storage/{}/{}'.format(t.key, t.filename))}">
                                                    <img style="width: 150px;" src="${tg.url('/storage/{}/{}'.format(t.key, t.filename))}">
                                                </a>

                                            </li>

                                %endfor

                        </ul>



                    <section class="description">
                        <h3>
                            توضیحات ${application.label}
                        </h3>

                            ${application.fa_description | n}

                    </section>


                    <section class="footer_singe_product">
                        <div class="col-md-4">
                            <div class="head_detail">
                                دسته بندی
                            </div>
                            <div class="text_detail">
                                ${application.categories.name}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="head_detail">
                                آخرین نسخه
                            </div>
                            <div class="text_detail">
                                ${latest_release.version}
                            </div>
                        </div> <div class="col-md-4">
                        <div class="head_detail">
                            تاریخ انتشار
                        </div>
                        <div class="text_detail">
                            ${JalaliDatetime(latest_release.created).strftime('%A %d %B %Y')}
                        </div>
                    </div> <div class="col-md-4">
                        <div class="head_detail">
                            نام بسته
                        </div>
                        <div class="text_detail">
                            ${application.package_name}
                        </div>
                    </div> <div class="col-md-4">
                        <div class="head_detail">
                            نسخه اندروید
                        </div>
                        <div class="text_detail">
                            ${latest_release.sdkVersion}
                        </div>
                    </div> <div class="col-md-4">
                        <div class="head_detail">
                            گزارش تخلف:
                        </div>
                        <div class="text_detail">
                            لینک گزارش تخلف
                        </div>
                    </div>
                    </section>

                    <section class="features">
                        <h3>
                            دسترسی ها
                        </h3>
                        %for p in normalize_permissions(latest_release.permissions):
                            <li>

                            <p>
                                <i class="fa fa-list"></i>
                                ${p}
                            </p>
                        </li>
                        %endfor



                    </section>
                </div>




            </div>
        </div>

        <div class="col-md-3 col-xs-12 ">
            <div class="sidebar">


            <div class="head_sidebar">

                <span class="part_right">
                    برنامه های مشابه
                </span>

            </div>

            <div class="body_sidebar">

                <ul>
                    %for app in similar:
                            <div class="box_products">
                    <li>
                        <a href="${tg.url('/applications/details/{}/{}/{}'.format(app.uid, app.package_name, space_to_dash(app.label)))}">

                            <img style="width: 150px;" src="${find_logo(app)}">

                            <div class="box_detail">
                                <p class="name_product">
                                    ${app.label}
                                </p>

                                <p class="price">
                                    رایگان
                                </p>

                                <p>

                                <div class="star_point">
                                    <div class="star-rating">
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="4">
                                    </div>
                                </div>
                                </p>
                            </div>
                        </a>
                    </li>
                </div>
                        %endfor
                </ul>


            </div>
            </div>

        </div>
    </div>

</div>


<script src="/index/js/thumbnail-slider.js"></script>

<script>
    var thumbnailSliderOptions =
{
    sliderId: "thumbnail-slider",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "200px",
    showMode: 1,
    autoAdvance: true,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};

##     var mcThumbnailSlider = new ThumbnailSlider(thumbnailSliderOptions);
</script>

