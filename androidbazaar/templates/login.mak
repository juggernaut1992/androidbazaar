<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="${tg.url('/plugins/images/favicon.png')}">
<title></title>
<!-- Bootstrap Core CSS -->
<link href="${tg.url('/plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css')}" rel="stylesheet">
<!-- animation CSS -->
<link href="${tg.url('/css/animate.css" rel="stylesheet')}">
<!-- Custom CSS -->
    <link href="${tg.url('/plugins/bower_components/toast-master/css/jquery.toast.css')}" rel="stylesheet">
<link href="${tg.url('/css/style.css')}" rel="stylesheet">
<!-- color CSS -->
<link href="${tg.url('/css/colors/blue.css')}" id="theme"  rel="stylesheet">

</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" action="index.html">
        <a class="text-center" href="${tg.url('/')}"> لوگو اندروید بازار</a>

        <div class="form-group m-t-40">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="نام کاربری" id="username">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="گذرواژه" id="password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> من را به خاطر بسپار </label>
            </div>
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> پسورد خود را فراموش کرده اید؟</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button onclick="login_($('#username').val(), $('#password').val())" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ورود</button>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>آیا اکانت کاربری ندارید؟<a href="${tg.url('/register')}" class="text-primary m-l-5"><b>ثبت نام</b></a></p>
          </div>
        </div>
      </form>
      <form class="form-horizontal" id="recoverform" action="index.html">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>بازیابی پسورد</h3>
            <p class="text-muted">ایمیل خود را وارد کنید و دستورالعمل ها به شما ارسال می شود! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">ریست</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="${tg.url('/plugins/bower_components/jquery/dist/jquery.min.js')}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${tg.url('/plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js')}"></script>
<!-- Menu Plugin JavaScript -->
<script src="${tg.url('/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}"></script>

<!--slimscroll JavaScript -->
<script src="${tg.url('/js/jquery.slimscroll.js')}"></script>
<!--Wave Effects -->
<script src="${tg.url('/js/waves.js')}"></script>
<!-- Custom Theme JavaScript -->
<script src="${tg.url('/js/custom.js')}"></script>
<!--Style Switcher -->
<script src="${tg.url('/plugins/bower_components/toast-master/js/jquery.toast.js')}"></script>
<script src="${tg.url('/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}"></script>
</body>
</html>

<script>
    function login_(login, password) {
    $('#loginBtn').prop('disabled', 'disabled');
    $.get(
        '${base_url}' + '/login_handler?login=' + login + '&password=' + password,
        function (resp) {
            debugger;
            if (resp == 'True') {

                window.location = '${base_url}' + '${came_from}'
            }
            else {
                $('#loginBtn').removeAttr('disabled');
                alertify('نام کاربری و یا رمز عبور اشتباه است');

            }
        }
    )
}
</script>