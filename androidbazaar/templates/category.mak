<%inherit file="local:templates.master"/>

<%
    def find_logo(application):
        logo = [t for t in application.thumbnails if t.variant == 'logo']
        logo_key = logo[0].key if logo else ''
        logo_filename = logo[0].filename if logo else ''
        return tg.url('/storage/{}/{}'.format(logo_key, logo_filename))

    def space_to_dash(s):
        return s.replace(' ', '-')
%>

<div id="thumbnail-slider">
    <div class="inner">
        <ul>
             %for app in applications:

                <div class="box_products">
                    <li>
                        <a href="${tg.url('/applications/details/{}/{}/{}'.format(app.uid, app.package_name, space_to_dash(app.label)))}">

                            <img style="width: 150px;" src="${find_logo(app)}">

                            <div class="box_detail">
                                <p class="name_product">
                                    ${app.label}
                                </p>

                                <p class="price">
                                    رایگان
                                </p>

                                <p>

                                <div class="star_point">
                                    <div class="star-rating">
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="4">
                                    </div>
                                </div>
                                </p>
                            </div>
                        </a>
                    </li>
                </div>
                %endfor


        </ul>
    </div>
</div>

<script src="/index/js/thumbnail-slider.js"></script>
<script>

var thumbnailSliderOptions =
{
    sliderId: "thumbnail-slider",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "200px",
    showMode: 1,
    autoAdvance: true,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};


var thumbnailSliderOptions2 =
{
    sliderId: "thumbnail-slider2",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "190px",
    showMode: 2,
    autoAdvance: false,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};


var thumbnailSliderOptions3 =
{
    sliderId: "thumbnail-slider3",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "190px",
    showMode: 2,
    autoAdvance: false,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};


var thumbnailSliderOptions4 =
{
    sliderId: "thumbnail-slider4",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "190px",
    showMode: 2,
    autoAdvance: false,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};





var thumbs2Op =
{
    sliderId: "thumbs2",
    orientation: "vertical",
    thumbWidth: "90px",
    thumbHeight: "auto",
    showMode: 3,
    autoAdvance: true,
    selectable: true,
    slideInterval: 2500,
    transitionSpeed: 800,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 100,
    keyboardNav: true,
    mousewheelNav: true,
    before: null,
    license: "mylicense"
};


var mcThumbnailSlider = new ThumbnailSlider(thumbnailSliderOptions);
## var mcThumbnailSlider2 = new ThumbnailSlider(thumbnailSliderOptions2);
## var mcThumbnailSlider3 = new ThumbnailSlider(thumbnailSliderOptions3);
## var mcThumbnailSlider4 = new ThumbnailSlider(thumbnailSliderOptions4);
## var mcThumbs2 = new ThumbnailSlider(thumbs2Op);
</script>