<%inherit file="local:templates.developer.master"/>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">نظرات برنامه ${application.label}</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">نظرات برنامه ${application.label}</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


<div class="col-md-12 col-lg-12 col-sm-12">
    <div class="panel">
    <div class="panel-heading">مشاهده نظرات</div>
    <div class="table-responsive">
        <table class="table table-hover manage-u-table">
            <thead>
                <tr>
                    <th width="70" class="text-center">#</th>
                    <th>نام کاربر</th>
                    <th>متن نظر</th>
                    <th>تاریخ ثبت نظر</th>
                    <th>درخواست حذف نظر</th>
                </tr>
            </thead>
            <tbody>
                %for c in comments:
                <tr>
                    <td class="text-center">${c.uid}</td>
                    <td><span class="font-medium">${c.tg_user.user_name}</span></td>
                    <td>${c.text}</td>
                    <td>${c.created}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-trash"></i></button>
                    </td>
                </tr>
                %endfor
            </tbody>
        </table>
    </div>
</div>
</div>