<%inherit file="local:templates.developer.master"/>


<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">فرم ثبت برنامه جدید</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">فرم ثبت برنامه جدید</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
    <div class="col-sm-6 app-submit-container col-xs-12">
        <div class="white-box p-l-20 p-r-20">
            <h3 class="box-title m-b-0">ثبت برنامه جدید</h3>
            <p class="text-muted m-b-30 font-13"></p>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-material form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-12">انتخاب دسته بندی</label>
                            <div class="col-sm-12">
                                <select class="form-control" id="app-category-id">
                                    %for c in categories:
                                        <option value="${c.uid}">${c.name}</option>
                                    %endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">آپلود فایل برنامه</label>
                            <div class="col-sm-12">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">انتخاب فایل</span> <span class="fileinput-exists">تغییر</span>
                                    <input id="app-apk" type="file" name="..."> </span> <a href="form-material-elements.html#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">حذف</a> </div>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                          <div class="col-xs-12">
                            <button id="submitBtn" onclick="submitApp($('#app-category-id').val(), $('#app-apk'))" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ثبت برنامه</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<script>
    function submitApp(category_id, apkFile) {

        if (apkFile[0].files.length == 0) {
            alertify('خطا', 'فایل برنامه را انتخاب کنید', 'error');
            return;
        }
        var formData = new FormData();
        formData.append('cat_id', category_id);
        var apk = apkFile[0].files[0];
        if (apk.size > 50000 * 1024) {
            alertify('خطا', 'حجم فایل حداکثر می تواند ۵۰ مگابایت باشد', 'error');
            return;
        }
        formData.append('apk', apk);
        var submitBtn = $('#submitBtn');
        submitBtn.prop('disabled', 'disabled');
        submitBtn.text('لطفا شکیبا باشید...');
        // Submit
        $.ajax({
           url : '${base_url}' + '/api/applications',
           type : 'POST',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
               window.location = '${base_url}' + '/developers/applications'
           },
           error : function(resp) {
                    submitBtn.removeAttr('disabled');
                    submitBtn.text('ثبت');
                    alertify(resp.responseJSON.detail)
        }});
    }
</script>