<%inherit file="local:templates.developer.master"/>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">انتخاب تصاویر برنامه</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">انتخاب تصاویر برنامه</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
    <div class="fileinput fileinput-new input-group col-sm-2 pull-right" data-provides="fileinput">
        <div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-success btn-file"> <span class="fileinput-new">تصویر جدید</span>
        <input onchange="submitThumbnail(this)" id="app-apk" type="file" name="..."> </span>
    </div>
</div>
<div class="row">
    %for t in [t for t in thumbnails if t.variant == 'thumbnail']:
        <div class="col-sm-3 ol-md-12 col-xs-12">
            <div class="white-box">
                <div class="thumbnail">
                    <a class="glyphicon glyphicon-remove" onclick="deleteThumbnail('${t.uid}')"></a>
                    <img src="${tg.url('/storage/{}/{}'.format(t.key, t.filename))}">
                </div>
            </div>
        </div>
    %endfor


</div>
<!-- /.row -->

<script>
    function deleteThumbnail(uid) {
        $.ajax({
           url : '${base_url}' + '/api/thumbnails/' + uid,
           type : 'DELETE',
           data : false,
           processData: false,
           contentType: false,
           success : function(resp) {
               location.reload()
           },
           error : function(resp) {
                alertify(resp.responseJSON.detail)
        }});
    }
    function submitThumbnail(file) {
        var formData = new FormData();
        formData.append('uid', '${app_uid}');
        formData.append('variant', 'thumbnail');
        formData.append('image', file.files[0]);
        // Submit
        $.ajax({
           url : '${base_url}' + '/api/thumbnails',
           type : 'POST',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
               location.reload()
           },
           error : function(resp) {
                alertify(resp.responseJSON.detail)
        }});

    }
</script>