<%inherit file="local:templates.developer.master"/>

<%
    logo = [t for t in application.thumbnails if t.variant == 'logo']
    logo_key = logo[0].key if logo else ''
    logo_filename = logo[0].filename if logo else ''
%>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">ویرایش اطلاعات ${application.label}</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">ویرایش اطلاعات ${application.label}</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title m-b-0">اطلاعات پایه</h3>
            <p class="text-muted m-b-30 font-13"></p>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-12">عنوان برنامه</label>
                    <div class="col-md-12">
                        <input id="label" type="text" class="form-control" value="${application.label}"> </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12">دسته بندی</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="cat-id">
                            %for c in categories:
                                <option value="${c.uid}" ${"selected" if application.category_id == c.uid else ""}>${c.name}</option>
                            %endfor
                        </select>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="white-box">
            <h3 class="box-title m-b-0">قیمت</h3>
            <p class="text-muted m-b-30 font-13"></p>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-12">قیمت فعلی</label>
                    <div class="col-md-12">
                        <input type="text" class="form-control" value="${application.price if application.price else 'رایگان'}" disabled> </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12">قیمت جدید</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="price">
                            <option value="0">رایگان</option>
                            <option value="1000">۱۰۰۰ تومان</option>
                            <option value="2000">۲۰۰۰ تومان</option>
                            <option value="3000">۳۰۰۰ تومان</option>
                        </select>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="white-box">
##             <h3 class="box-title m-b-0">تصویر</h3>
##             <p class="text-muted m-b-30 font-13"></p>
            <form class="form-horizontal">
                <div class="form-group" style="margin-bottom: 0;">
                    <div class="thumbnail">
                        <img src="${tg.url('/storage/{}/{}'.format(logo_key, logo_filename)) if logo_key else ''}">
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 0">
                    <div class="col-sm-12">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput" style="margin-bottom: 0">
                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">تغییر لوگو</span> <span class="fileinput-exists">تغییر</span>
                        <input id="logo-btn" type="file" name="..."> </span> <a href="form-material-elements.html#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">حذف</a> </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
                <!-- /.row -->

<!-- .row -->
<div class="row">
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">توضیحات فارسی</h3>
            <div id="fa-description">
                <h6>${application.fa_description or '' | n}</h6> </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">توضیحات انگلیسی</h3>
            <div id="en-description">
                <h6>${application.en_description or '' | n}</h6> </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 login-container">
        <button id="submitBtn" onclick="submitDetails($('#en-description').summernote().code(), $('#fa-description').summernote().code(), $('#price').val(), $('#label').val(), $('#cat-id').val())" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ثبت تغییرات</button>
    </div>
</div>
<!-- /.row -->
<script src="${tg.url('/plugins/bower_components/summernote/dist/summernote.min.js')}"></script>
<script>
      $(function() {
     $("#logo-btn").change(function (){
        var pImage = $("#logo-btn")[0].files[0];
        if (pImage == undefined) {
            return;
        }
	    if (pImage.size > 1000 * 1024) {
		    alertify("سایز تصویر حداکثر باید ۱ مگابایت باشد");
	    	return;
    	}
       var formData = new FormData();
       formData.append('image', pImage);
       formData.append('variant', 'logo');
       formData.append('uid', '${application.uid}');
         // Submit
        $.ajax({
           url : '${base_url}' + '/api/thumbnails',
           type : 'POST',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
                if (resp.ok) {
                    location.reload();
                }
                else {
                    alertify(resp.error);
                }
           }
    });


     });
  });

    $(document).ready(function () {
        $('#fa-description').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
         $('#en-description').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false
                // set focus to editable area after initializing summernote
            });
    });

    function submitDetails(en_desc, fa_desc, price, label, cat_id) {
        var formData = new FormData();
        formData.append('en_description', en_desc);
        formData.append('fa_description', fa_desc);
        formData.append('price', price);
        formData.append('label', label);
        formData.append('cat_id', cat_id);
        var submitBtn = $('#submitBtn');
        submitBtn.prop('disabled', 'disabled');
        submitBtn.text('لطفا شکیبا باشید...');

        $.ajax({
           url : '${base_url}' + '/api/applications/' + '${application.uid}',
           type : 'PUT',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
               location.reload()
           },
           error : function(resp) {
                    submitBtn.removeAttr('disabled');
                    submitBtn.text('ثبت تغییرات');
                    alertify(resp.responseJSON.detail)
        }});
    }
</script>