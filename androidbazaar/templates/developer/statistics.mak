<%inherit file="local:templates.developer.master"/>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">آمار فروش و بازدید برنامه ${application.label}</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">آمار فروش و بازدید برنامه ${application.label}</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">آمار فروش</h3>
            <div>
                <canvas id="salesReport" height="150"></canvas>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">آمار بازدید</h3>
            <div>
                <canvas id="viewReport" height="150"></canvas>
            </div>
        </div>
    </div>

</div>
<div>
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">آمار حذف\نصب</h3>
            <div>
                <canvas id="installsReport" height="150"></canvas>
            </div>
        </div>
    </div>

</div>


<script>
    $( document ).ready(function() {

        $.get('${base_url}' + '/api/reports/sale/' + ${application.uid},
                function(resp) {
                    var keys = [];
                    var values = [];
                    for (var key in resp.data) {
                        keys.push(key);
                        values.push(resp.data[key]);
                    }
                    var ctx1 = document.getElementById("salesReport").getContext("2d");
                    var data1 = {
                        labels: keys,
                        datasets: [
                            {
                                label: "My Second dataset",
                                fillColor: "#e064ae",
                                strokeColor: "#e064ae",
                                pointColor: "rgba(243,245,246,0.9)",
                                pointStrokeColor: "#e064ae",
                                pointHighlightFill: "#e064ae",
                                pointHighlightStroke: "rgba(243,245,246,0.9)",
                                data: values
                            },
                        ]
                    };

                    new Chart(ctx1).Line(data1, {
                        scaleShowGridLines : true,
                        scaleGridLineColor : "rgba(0,0,0,.005)",
                        scaleGridLineWidth : 0,
                        scaleShowHorizontalLines: true,
                        scaleShowVerticalLines: true,
                        bezierCurve : true,
                        bezierCurveTension : 0.4,
                        pointDot : true,
                        pointDotRadius : 4,
                        pointDotStrokeWidth : 1,
                        pointHitDetectionRadius : 2,
                        datasetStroke : true,
                        tooltipCornerRadius: 2,
                        datasetStrokeWidth : 2,
                        datasetFill : false,
                        legendTemplate : ${"'<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'" | n},
                        responsive: true
                    });
                }
        );

        // Views


        $.get('${base_url}' + '/api/reports/view/' + ${application.uid},
                function(resp) {
                    var keys = [];
                    var values = [];
                    for (var key in resp.data) {
                        keys.push(key);
                        values.push(resp.data[key]);
                    }
                    var ctx1 = document.getElementById("viewReport").getContext("2d");
                    var data1 = {
                        labels: keys,
                        datasets: [
                            {
                                label: "My Second dataset",
                                fillColor: "#64d9e0",
                                strokeColor: "#64d9e0",
                                pointColor: "rgba(243,245,246,0.9)",
                                pointStrokeColor: "#64d9e0",
                                pointHighlightFill: "#64d9e0",
                                pointHighlightStroke: "rgba(243,245,246,0.9)",
                                data: values
                            },
                        ]
                    };

                    new Chart(ctx1).Line(data1, {
                        scaleShowGridLines : true,
                        scaleGridLineColor : "rgba(0,0,0,.005)",
                        scaleGridLineWidth : 0,
                        scaleShowHorizontalLines: true,
                        scaleShowVerticalLines: true,
                        bezierCurve : true,
                        bezierCurveTension : 0.4,
                        pointDot : true,
                        pointDotRadius : 4,
                        pointDotStrokeWidth : 1,
                        pointHitDetectionRadius : 2,
                        datasetStroke : true,
                        tooltipCornerRadius: 2,
                        datasetStrokeWidth : 2,
                        datasetFill : false,
                        legendTemplate : ${"'<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'" | n},
                        responsive: true
                    });
                }
        );

        // Install/UnInstall

        $.get('${base_url}' + '/api/reports/installs/' + ${application.uid},
                function(resp) {
                    var keys = [];
                    var installs = [];
                    var uninstalls = [];
                    for (var key in resp.installs_data) {
                        keys.push(key);
                        installs.push(resp.installs_data[key]);
                    }
                    for (var key in resp.uninstalls_data) {
                        uninstalls.push(resp.uninstalls_data[key]);
                    }
                    var ctx1 = document.getElementById("installsReport").getContext("2d");
                    var data1 = {
                        labels: keys,
                        datasets: [
                            {
                                label: "My Second dataset",
                                fillColor: "#e064ae",
                                strokeColor: "#e064ae",
                                pointColor: "rgba(243,245,246,0.9)",
                                pointStrokeColor: "#e064ae",
                                pointHighlightFill: "#e064ae",
                                pointHighlightStroke: "rgba(243,245,246,0.9)",
                                data: installs
                            },
                            {
                                label: "My Second dataset",
                                fillColor: "#5990e0",
                                strokeColor: "#e064ae",
                                pointColor: "rgba(243,245,246,0.9)",
                                pointStrokeColor: "#e064ae",
                                pointHighlightFill: "#e064ae",
                                pointHighlightStroke: "rgba(243,245,246,0.9)",
                                data: uninstalls
                            },
                        ]
                    };

                    new Chart(ctx1).Line(data1, {
                        scaleShowGridLines : true,
                        scaleGridLineColor : "rgba(0,0,0,.005)",
                        scaleGridLineWidth : 0,
                        scaleShowHorizontalLines: true,
                        scaleShowVerticalLines: true,
                        bezierCurve : true,
                        bezierCurveTension : 0.4,
                        pointDot : true,
                        pointDotRadius : 4,
                        pointDotStrokeWidth : 1,
                        pointHitDetectionRadius : 2,
                        datasetStroke : true,
                        tooltipCornerRadius: 2,
                        datasetStrokeWidth : 2,
                        datasetFill : false,
                        legendTemplate : ${"'<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'" | n},
                        responsive: true
                    });
                }
        );



    });
</script>
