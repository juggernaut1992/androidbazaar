<%inherit file="local:templates.developer.master"/>
<%
    def space_to_dash(s):
        return s.replace(' ', '-')
%>


<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">برنامه های من</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="index.html#">پنل مدیریت</a></li>
            <li class="active">برنامه های من</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- ============================================================== -->
<!-- wallet, & manage users widgets -->
<!-- ============================================================== -->
<!-- .row -->
<div class="row">
     <!-- col-md-9 -->
        <div class="col-md-8 col-lg-9">
            <div class="manage-users">
                <div class="sttabs tabs-style-iconbox">
                    <nav>
                        <ul>
                            <li><a class="sticon ti-user"><span>برنامه های من</span></a></li>
                        </ul>
                    </nav>
                    <div class="content-wrap">
                        <section id="section-iconbox-1">
                            <div class="p-20 row">
                                <div class="col-sm-6">
                                    <h3 class="m-t-0">برنامه های خود را مدیریت کنید</h3></div>
                                <div class="col-sm-6">
                                    <ul class="side-icon-text pull-right">
                                        <li><a href="${tg.url('/developers/new')}"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>افزودن برنامه</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="table-responsive manage-table">
                                <table class="table" cellspacing="14">
                                    <thead>
                                        <tr>
                                            <th width="150">نام</th>
                                            <th>قیمت</th>
                                            <th>تعداد نصب</th>
                                            <th>فروش</th>
                                            <th>وضعیت</th>
##                                             <th>تصاویر</th>
##                                             <th>نسخه ها</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        %for a in applications:
                                            <tr class="advance-table-row">
                                                <td><a href="${tg.url('/developers/application/{}/{}/{}'.format(a.uid, a.package_name, space_to_dash(a.label)))}">${a.label}</a></td>
                                                <td>${a.price} تومان </td>
                                                <td>${len(a.installs)}</td>
                                                <td>${len(a.installs) * a.price} تومان </td>
                                                <td>${a.latest_release().status}</td>
                                                <td><a href="${tg.url('/developers/thumbnails/{}/{}/{}'.format(a.uid, a.package_name, space_to_dash(a.label)))}">تصاویر</a></td>
                                                <td><a href="${tg.url('/developers/releases/{}/{}/{}'.format(a.uid, a.package_name, space_to_dash(a.label)))}">نسخه ها</a></td>
                                                <td><a href="${tg.url('/developers/statistics/{}/{}/{}'.format(a.uid, a.package_name, space_to_dash(a.label)))}">آمار</a></td>
                                                <td><a href="${tg.url('/developers/comments/{}/{}/{}'.format(a.uid, a.package_name, space_to_dash(a.label)))}">نظرات</a></td>
                                            </tr>
                                        %endfor
                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                    <!-- /content -->
                </div>
                <!-- /tabs -->
            </div>
        </div>
        <!-- /col-md-9 -->
    <!-- col-md-3 -->
    <div class="col-md-4 col-lg-3">
        <div class="panel wallet-widgets">
            <div class="panel-body">
                <ul class="side-icon-text">
                    <li class="m-0"><a href="index.html#"><span class="circle circle-md bg-success di vm"><i class="ti-plus"></i></span><span class="di vm"><h1 class="m-b-0">${user.balance} تومان</h1><h5 class="m-t-0">موجودی کیف پول شما</h5></span></a></li>
                </ul>
            </div>
            <div id="morris-area-chart2" style="height:208px"></div>
            <ul class="wallet-list">
                <li><i class=" ti-wallet"></i><a href="${tg.url('/developers/withdraw')}">درخواست تسویه حساب</a></li>

            </ul>
        </div>
    </div>
    <!-- /col-md-3 -->
</div>


<script src="${tg.url('/js/cbpFWTabs.js')}"></script>
<script src="${tg.url('/js/dashboard1.js')}"></script>
<script type="text/javascript">
        (function () {
                [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
                new CBPFWTabs(el);
            });
        })();
    </script>