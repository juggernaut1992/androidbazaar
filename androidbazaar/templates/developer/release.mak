<%inherit file="local:templates.developer.master"/>
<%
    from khayyam import JalaliDatetime
    def space_to_dash(s):
        return s.replace(' ', '-')
%>


<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">بسته های برنامه ${app_title}</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">بسته های برنامه ${app_title}</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- ============================================================== -->
<!-- wallet, & manage users widgets -->
<!-- ============================================================== -->
<!-- .row -->
<div class="row">
     <!-- col-md-9 -->
        <div class="col-md-12 col-lg-12">
            <div class="manage-users">
                <div class="sttabs tabs-style-iconbox">
                    <nav>
                        <ul>
                            <li><a class="sticon ti-user"><span>بسته های برنامه ${app_title}</span></a></li>
                        </ul>
                    </nav>
                    <div class="content-wrap">
                        <section id="section-iconbox-1">
                            <div class="p-20 row">
                                <div class="col-sm-8">
                                    <h4 class="m-t-0">نسخه های برنامه ${app_title} را مدیریت کنید</h4></div>
                                <div class="col-sm-4">
                                    <ul class="side-icon-text pull-right">
                                        <li><a href="${tg.url('/developers/release/{}/{}/{}'.format(app_uid, package_name, space_to_dash(app_title)))}"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>افزودن بسته جدید</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="table-responsive manage-table">
                                <table class="table" cellspacing="14">
                                    <thead>
                                        <tr>
                                            <th>شناسه</th>
                                            <th>نسخه</th>
                                            <th>حجم ~</th>
                                            <th>تاریخ ثبت</th>
                                            <th>تغییرات بسته</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        %for r in releases:
                                            <tr class="advance-table-row">
                                                <td>${r.uid}</td>
                                                <td>${r.version}</td>
                                                <td>${r.size}</td>
                                                <td>${JalaliDatetime(r.created).strftime('%A %d %B %Y %H:%M')}</td>
                                                <td><a href="${tg.url('/developers/changelog/{}/{}/{}'.format(r.uid, package_name, space_to_dash(app_title)))}">مشاهده و تغییر</a></td>
                                            </tr>
                                        %endfor
                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                    <!-- /content -->
                </div>
                <!-- /tabs -->
            </div>
        </div>
        <!-- /col-md-9 -->
    <!-- col-md-3 -->
    <!-- /col-md-3 -->
</div>


<!-- /.container-fluid -->
<script src="${tg.url('/js/cbpFWTabs.js')}"></script>
<script type="text/javascript">
        (function () {
                [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
                new CBPFWTabs(el);
            });
        })();
    </script>