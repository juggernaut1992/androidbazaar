<%inherit file="local:templates.developer.master"/>
<%
    def space_to_dash(s):
        return s.replace(' ', '-')
%>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">ویرایش تغییرات نسخه ${release.version} برنامه ${app_title}</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li><a href="${tg.url('/developers/releases/{}/{}/{}'.format(app_uid, package_name, space_to_dash(app_title)))}">نسخه های برنامه ${app_title}</a></li>
            <li class="active">ویرایش تغییرات نسخه ${release.version} برنامه ${app_title}</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

                <!-- /.row -->

<!-- .row -->
<div class="row">
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">تغییرات نسخه به زبان فارسی</h3>
            <div id="fa-log">
                <h6>${release.fa_log or '' | n}</h6> </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">تغییرات نسخه به زبان انگلیسی</h3>
            <div id="en-log">
                <h6>${release.en_log or '' | n}</h6> </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 login-container">
        <button id="submitBtn" onclick="submitChangelog($('#en-log').summernote().code(), $('#fa-log').summernote().code())" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ثبت تغییرات</button>
    </div>
</div>
<script src="${tg.url('/plugins/bower_components/summernote/dist/summernote.min.js')}"></script>
<script>
    $(document).ready(function () {
        $('#fa-log').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
         $('#en-log').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false
                // set focus to editable area after initializing summernote
            });
    });

    function submitChangelog(enLog, faLog) {
        var formData = new FormData();
        formData.append('en_log', enLog);
        formData.append('fa_log', faLog);

        var submitBtn = $('#submitBtn');
        submitBtn.prop('disabled', 'disabled');
        submitBtn.text('لطفا شکیبا باشید...');

        $.ajax({
           url : '${base_url}' + '/api/releases/' + '${release.uid}',
           type : 'PUT',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
               location.reload()
           },
           error : function(resp) {
                    submitBtn.removeAttr('disabled');
                    submitBtn.text('ثبت تغییرات');
                    alertify(resp.responseJSON.detail)
        }});
    }
</script>