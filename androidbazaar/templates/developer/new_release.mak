<%inherit file="local:templates.developer.master"/>
<%
    from khayyam import JalaliDatetime
    def space_to_dash(s):
        return s.replace(' ', '-')
%>


<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">فرم ثبت بسته جدید برنامه ${application.label}</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active"><a href="${tg.url('/developers/releases/{}/{}/{}'.format(application.uid, application.package_name, space_to_dash(application.label)))}">نسخه های برنامه ${application.label}</a></li>
            <li class="active">فرم ثبت بسته جدید برنامه ${application.label}</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="p-l-20 p-r-20">
            <h3 class="box-title m-b-0">ثبت بسته جدید</h3>
            <p class="text-muted m-b-30 font-13"></p>

            <div class="row">
                <div class="col-sm-4">
                    <div class="white-box">
                        <h3 class="box-title">تغییرات بسته به زبان فارسی</h3>
                        <div id="fa-log">
                            <h6></h6> </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="white-box">
                        <h3 class="box-title">تغییرات بسته به زبان انگلیسی</h3>
                        <div id="en-log">
                            <h6></h6> </div>
                    </div>
                </div>
                <div class="col-sm-4 white-box">
                        <div class="col-sm-12">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-group">
                            <label class="col-sm-12">آپلود فایل بسته</label>
                            <div class="col-sm-12">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">انتخاب فایل</span> <span class="fileinput-exists">تغییر</span>
                                    <input id="app-apk" type="file" name="..."> </span> <a href="form-material-elements.html#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">حذف</a> </div>
                            </div>
                        </div>
                        </div>
                    <div class="col-md-12">
                        <div class="form-group text-center m-t-20">
                          <div class="col-xs-12">
                            <button id="submitBtn" onclick="submitRelease($('#app-apk'), $('#en-log').summernote().code(), $('#fa-log').summernote().code())" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ثبت بسته جدید</button>
                          </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
</div>
<!-- /.row -->
<script src="${tg.url('/plugins/bower_components/summernote/dist/summernote.min.js')}"></script>
<script>
    $(document).ready(function () {
        $('#fa-log').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
         $('#en-log').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false
                // set focus to editable area after initializing summernote
            });
    });
    function submitRelease(apkFile, en_log, fa_log) {

        if (apkFile[0].files.length == 0) {
            alertify('فایل برنامه را انتخاب کنید');
            return;
        }
        var formData = new FormData();
        var apk = apkFile[0].files[0];
        if (apk.size > 50000 * 1024) {
            alertify('حجم فایل حداکثر می تواند ۵۰ مگابایت باشد');
            return;
        }
        formData.append('apk', apk);
        formData.append('fa_log', fa_log);
        formData.append('en_log', en_log);
        var submitBtn = $('#submitBtn');
        submitBtn.prop('disabled', 'disabled');
        submitBtn.text('لطفا شکیبا باشید...');
        // Submit
        $.ajax({
           url : '${base_url}' + '/api/releases/' + '${application.uid}',
           type : 'POST',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
               window.location = '${base_url}' + '/developers/releases/' + '${application.uid}' + '/' + '${application.package_name}' + '/' + '${space_to_dash(application.label)}'
           },
           error : function(resp) {
                    submitBtn.removeAttr('disabled');
                    submitBtn.text('ثبت');
                    alertify(resp.responseJSON.detail)
        }});
    }
</script>