<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Amin Etesamian">
    <link rel="icon" type="image/png" sizes="16x16" href="${tg.url('/plugins/images/favicon.png')}">
    <title>اندروید بازار</title>
    <!-- Bootstrap Core CSS -->
    <link href="${tg.url('/plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css')}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="${tg.url('/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}" rel="stylesheet">
##     <link href="${tg.url('/plugins/bower_components/chartist-js/dist/chartist.min.css')}" rel="stylesheet">
    <!-- toast CSS -->
    <!-- animation CSS -->
    <link href="${tg.url('/css/animate.css')}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="${tg.url('/css/style.css')}" rel="stylesheet">
    <link href="${tg.url('/plugins/bower_components/morrisjs/morris.css')}" rel="stylesheet">
    <!-- color CSS -->
    <link href="${tg.url('/css/colors/blue-dark.css')}" id="theme" rel="stylesheet">
    <link href="${tg.url('/plugins/bower_components/owl.carousel/owl.carousel.min.css')}" rel="stylesheet" type="text/css" />
    <link href="${tg.url('/plugins/bower_components/sweetalert/sweetalert.css')}" rel="stylesheet" type="text/css" />
    <link href="${tg.url('/plugins/bower_components/toast-master/css/jquery.toast.css')}" rel="stylesheet">
    <link href="${tg.url('/plugins/bower_components/summernote/dist/summernote.css')}" rel="stylesheet" />
    <link rel="stylesheet" href="${tg.url('/plugins/bower_components/dropify/dist/css/dropify.min.css')}">
    <link href="${tg.url('/plugins/bower_components/chartist-js/dist/chartist.min.css')}" rel="stylesheet">
    <link href="${tg.url('/plugins/bower_components/chartist-js/dist/chartist-init.css')}" rel="stylesheet">
    <link href="${tg.url('/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>


<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0" style="padding-right: 0;">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="${tg.url('/')}">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon--><img src="${tg.url('/plugins/images/admin-logo.png')}" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="${tg.url('/plugins/images/admin-logo-dark.png')}" alt="home" class="light-logo" />
                     </b>
                        <!-- Logo text image you can use text also --><span class="hidden-xs">
##                         <!--This is dark logo text--><img src="../plugins/images/admin-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="../plugins/images/admin-text-dark.png" alt="home" class="light-logo" />
##                         <a>اندروید بازار</a>
                     </span> </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
                <ul class="nav navbar-top-links navbar-left">
                    <!-- .Task dropdown -->
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>

                    <li> <a class="waves-effect waves-light hidden-xs" href="${tg.url('/developers/applications')}"><span class="hidden-xs">پنل توسعه دهنده</span></a>
                    <li> <a class="waves-effect waves-light hidden-xs" href="#"><span class="hidden-xs">فرصت های شغلی</span></a>
                    <li> <a class="waves-effect waves-light hidden-xs" href="#"><span class="hidden-xs">ارتباط با ما</span></a>
                    <!-- /.Megamenu -->
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-sm m-r-10">
                            <input type="text" placeholder="جستجو ..." class="form-control"> <a href="#"><i class="fa fa-search"></i></a> </form>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic hidden-xs" data-toggle="dropdown" href="##"> <img src="${tg.url('/plugins/images/users/varun.jpg')}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"></b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="${tg.url('/plugins/images/users/varun.jpg')}" alt="user" /></div>
                                    <div class="u-text">
                                        <h4>امین اعتصامیان</h4>
                                        <p class="text-muted"></p><a href="#" class="btn btn-rounded btn-danger btn-sm">دیدن پروفایل</a></div>
                                </div>
                            </li>
##                             <li role="separator" class="divider"></li>
##                             <li><a href="index.html#"><i class="ti-user"></i> پروفایل من</a></li>
##                             <li><a href="index.html#"><i class="ti-wallet"></i> درآمد من</a></li>
##                             <li><a href="index.html#"><i class="ti-email"></i> صندوق ورودی</a></li>
##                             <li role="separator" class="divider"></li>
##                             <li><a href="index.html#"><i class="ti-settings"></i> تنظیمات حساب</a></li>
##                             <li role="separator" class="divider"></li>
##                             <li><a href="index.html#"><i class="fa fa-power-off"></i> خروج</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>

                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
                <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">منو</span></h3> </div>
                <ul class="nav" id="side-menu">
##                     <li class="user-pro">
##                         <a href="index.html#" class="waves-effect"><img src="${tg.url('/plugins/images/users/varun.jpg')}" alt="user-img" class="img-circle"> <span class="hide-menu"> ${}<span class="fa arrow"></span></span>
##                         </a>
##                         <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
##                             <li><a href="javascript:void(0)"><i class="ti-user"></i> <span class="hide-menu">پروفایل من</span></a></li>
##                             <li><a href="javascript:void(0)"><i class="ti-wallet"></i> <span class="hide-menu">درآمد من</span></a></li>
##                             <li><a href="javascript:void(0)"><i class="ti-email"></i> <span class="hide-menu">صندوق ورودی</span></a></li>
##                             <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">تنظیمات حساب</span></a></li>
##                             <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> <span class="hide-menu">خروج</span></a></li>
##                         </ul>
##                     </li>
                    <li> <a href="#" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> داشبورد <span class="fa arrow"></span> <span class="label label-rouded label-inverse pull-right">3</span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="${tg.url('/developers/applications')}"><i class="mdi mdi-android fa-fw"></i><span class="hide-menu">برنامه ها</span></a> </li>
                            <li> <a href="${tg.url('/developers/withdraw')}"><i class="ti-wallet fa-fw"></i><span class="hide-menu">درخواست تسویه حساب</span></a> </li>
                            <li> <a href="${tg.url('/developers/withdraws')}"><i class="fa-fw glyphicon glyphicon-credit-card"></i><span class="hide-menu">تسویه حساب ها</span></a> </li>
                        </ul>
                    </li>

                    <li><a href="${tg.url('/logout_handler')}" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">خروج</span></a></li>
                    <li class="devider"></li>
                    <li><a href="documentation.html" class="waves-effect"><i class="fa fa-circle-o text-danger"></i> <span class="hide-menu">مستندات</span></a></li>
                    <li><a href="gallery.html" class="waves-effect"><i class="fa fa-circle-o text-info"></i> <span class="hide-menu">گالری</span></a></li>
                    <li><a href="faq.html" class="waves-effect"><i class="fa fa-circle-o text-success"></i> <span class="hide-menu">پرسش های متداول</span></a></li>
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <script src="${tg.url('/plugins/bower_components/jquery/dist/jquery.min.js')}"></script>
        <div id="page-wrapper">
            <div class="container-fluid">
                ${next.body()}
                <footer class="footer text-center"> 1396 &copy; تمامی حقوق این برنامه برای اندروید بازار محفوظ است. </footer>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

    <!-- Bootstrap Core JavaScript -->
<script src="${tg.url('/plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js')}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="${tg.url('/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}"></script>
    <!--slimscroll JavaScript -->
    <script src="${tg.url('/js/jquery.slimscroll.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/owl.carousel/owl.carousel.min.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/owl.carousel/owl.custom.js')}"></script>
##     <script src="${tg.url('/plugins/bower_components/chartist-js/dist/chartist.min.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/sweetalert/sweetalert.min.js')}"></script>
    <!--Wave Effects -->
    <!-- Custom Theme JavaScript -->

    <script src="${tg.url('/js/custom.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/toast-master/js/jquery.toast.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/raphael/raphael-min.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/morrisjs/morris.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/Chart.js/Chart.min.js')}"></script>
<!-- Custom tab JavaScript -->
    <script src="${tg.url('/js/cbpFWTabs.js')}"></script>
    <script src="${tg.url('/plugins/bower_components/dropify/dist/js/dropify.min.js')}"></script>
</body>

</html>