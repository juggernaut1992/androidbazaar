<%inherit file="local:templates.developer.master"/>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">درخواست تسویه حساب</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
        <a href="javascript:void(0)" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">چگونه درآمدم افزایش دهم؟</a>
        <ol class="breadcrumb">
            <li><a href="${tg.url('/developers/applications')}">پنل مدیریت</a></li>
            <li class="active">درخواست تسویه حساب</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
            <h3 class="box-title m-b-0">فرم درخواست تسویه حساب</h3>
            <p class="text-muted m-b-30 font-13">پس از تکمیل فرم همکاران ما در سریع ترین زمان ممکن وجه مورد نظر را به حساب شما واریز خواهند کرد.</p>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-material form-horizontal">
                        <div class="form-group">
                            <label class="col-md-12">مبلغ (تومان)</label>
                            <div class="col-md-12">
                                <input id="amount" type="number" class="form-control form-control-line" placeholder="موجودی شما : ${user.balance} تومان "> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">نام صاحب حساب</label>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control form-control-line" placeholder=""> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-email">شماره حساب</label>
                            <div class="col-md-12">
                                <input type="number" id="card" name="example-email" class="form-control" placeholder="..."> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">توضیحات (اختیاری)</label>
                            <div class="col-md-12">
                                <textarea id="description" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">تصویر کارت ملی</label>
                            <div class="col-sm-12">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">انتخاب فایل</span> <span class="fileinput-exists">تغییر</span>
                                    <input id="image" type="file" name="..."> </span> <a href="form-material-elements.html#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">حذف</a> </div>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-12">
                            <button id="submitBtn" onclick="submitWithdrawRequest($('#amount').val(), $('#name').val(), $('#card').val(), $('#description').val(), $('#image'))" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ثبت درخواست تسویه</button>
                          </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function submitWithdrawRequest(amount, holderName, holderCartNumber, description, image) {
        if (image[0].files.length == 0 || amount == "" || holderCartNumber == "" || holderName == "") {
            alertify('لطفا همه فیلد ها را به درستی پر کنید');
            return
        }
        var formData = new FormData();
        formData.append('amount', amount);
        formData.append('holderName', holderName);
        formData.append('holderCartNumber', holderCartNumber);
        formData.append('description', description);
        formData.append('image', image[0].files[0]);

        var submitBtn = $('#submitBtn');
        submitBtn.prop('disabled', 'disabled');
        submitBtn.text('لطفا شکیبا باشید...');
        // Submit
        $.ajax({
           url : '${base_url}' + '/api/withdraws/',
           type : 'POST',
           data : formData,
           processData: false,
           contentType: false,
           success : function(resp) {
               window.location = '${base_url}' + '/developers/withdraws/'
           },
           error : function(resp) {
                    submitBtn.removeAttr('disabled');
                    submitBtn.text('ثبت');
                    alertify(resp.responseJSON.detail)
        }});

    }
</script>