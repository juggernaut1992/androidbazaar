
<%
    from androidbazaar.model import Category, User
    categories = Category.all()
%>
<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>
اندروید بازار
    </title>
    <link rel="stylesheet" href="/index/css/bootstrap.min.css">
    <link rel="stylesheet" href="/index/css/style.css">
    <link rel="stylesheet" href="/index/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/index/css/thumbnail-slider.css" type="text/css">
    <script src="/index/js/jquery.min.js"></script>

</head>


<body>


<header>
    <div class="top_header">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
						<span id="logo">
							اندروید بازار
						</span>
                </div>

                <div class="col-md-5">
                    <form action="${tg.url('/search')}">
                        <div class="form-group search_box">
                            <input name="query" type="text" class="form-control" id="search" placeholder="جست و جو در برنامه ها">
                            <span id="search_icon"><i class="fa fa-search"></i></span>
                        </div>
                    </form>
                </div>

                <div class="col-md-2">
                    <div class="btn_get_bazzar">
                        دریافت اندروید بازار
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="bottom_header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-6">
						<span id="menu">
                            <ul>
                                <li>
                                    <a href="#" class="main_menu">
                                        <span class="caret"></span>
                                        دسته بندی ها
                                    </a>
                                    <ul class="sub-menu">
                                        <div class="col">
                                            <ul>
                                                %for c in categories[:7]:
                                                    <a href="${tg.url('/category/{}/{}'.format(c.uid, c.name))}">
                                                        <li>${c.name}</li>
                                                    </a>
                                                %endfor

                                            </ul>
                                        </div>
                                        <div class="col">
                                            <ul>
                                                %for c in categories[7:14]:
                                                    <a href="">
                                                        <li>${c.name}</li>
                                                    </a>
                                                %endfor

                                            </ul>
                                        </div>
                                        <div class="col">
                                            <ul>
                                                %for c in categories[14:]:
                                                    <a href="">
                                                        <li>${c.name}</li>
                                                    </a>
                                                %endfor

                                            </ul>
                                        </div>

                                    </ul>
                                </li>

                            </ul>
						</span>
                </div>

                <div class="col-md-6 col-xs-6">
                    <div class="login">
                        % if not User.current():
                            <a href="${tg.url('/login')}" id="">
								ورود \ ثبت نام
							</a>
                        %else:
                            <a href="${tg.url('/developers/applications')}" id="">
								حساب کاربری
							</a>
                        % endif
                    </div>
                </div>


            </div>
        </div>

    </div>

</header>

    ${next.body()}


            <footer>

                    <div class="container">
                        <div class="footer_top">
                            <div class="row">
                                <div class="head_footer">
                                    جستوجو های اخیر
                                </div>
                                <div class="col-md-3">
                                    <ul class="menu_footer">
                                        <li><a href="">تقویم فارسی</a></li>
                                        <li><a href="">تلگرام همه کاره</a></li>
                                        <li><a href="">خلافی ماشین</a></li>
                                        <li><a href="">کیبورد فارسی</a></li>
                                        <li><a href="">اسنپ</a></li>
                                        <li><a href="">ماشین سواری</a></li>
                                        <li><a href="">هواپیما</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <ul class="menu_footer">
                                        <li><a href="">تقویم فارسی</a></li>
                                        <li><a href="">تلگرام همه کاره</a></li>
                                        <li><a href="">خلافی ماشین</a></li>
                                        <li><a href="">کیبورد فارسی</a></li>
                                        <li><a href="">اسنپ</a></li>
                                        <li><a href="">ماشین سواری</a></li>
                                        <li><a href="">هواپیما</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="menu_footer">
                                        <li><a href="">تقویم فارسی</a></li>
                                        <li><a href="">تلگرام همه کاره</a></li>
                                        <li><a href="">خلافی ماشین</a></li>
                                        <li><a href="">کیبورد فارسی</a></li>
                                        <li><a href="">اسنپ</a></li>
                                        <li><a href="">ماشین سواری</a></li>
                                        <li><a href="">هواپیما</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>


                        <div class="footer_bottom">
                            <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="head_footer">
                                        دسته بندی ها
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="menu_footer">
                                            %for c in categories[:7]:
                                                <li><a href="">${c.name}</a></li>
                                            %endfor
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="menu_footer">
                                            %for c in categories[7:14]:
                                                <li><a href="">${c.name}</a></li>
                                            %endfor
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="menu_footer">
                                            %for c in categories[14:]:
                                                <li><a href="">${c.name}</a></li>
                                            %endfor
                                        </ul>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="head_footer">
                                        همکاری با ما
                                    </div>
                                        <ul class="menu_footer">
                                            <li><a href="">Cooperation and Else</a></li>
                                            <li><a href="">9apps_business</a></li>
                                            <li><a href="">business</a></li>
                                            <li><a href="">Cooperation</a></li>
                                        </ul>
                                </div>
                                <div class="col-md-3 social">
                                    <div class="head_footer">
                                        ارتباط با ما
                                    </div>
                                        <ul class="social_footer">
                                            <li><a href=""><img src="/index/img/socail1.jpg" alt=""></a></li>
                                            <li><a href=""><img src="/index/img/socail2.jpg" alt=""></a></li>
                                            <li><a href=""><img src="/index/img/socail3.jpg" alt=""></a></li>

                                        </ul>
                                </div>
                            </div>
                        </div>
                        </div>



                    <div class="copy_right">
                        <div class="logo_footer">
                            اندروید بازار
                        </div>
                        <div class="lAN_footer">
##                             <ul>
##                                 <li><a href="">English</a></li>
##                                 <li><a href="">Indonesia</a></li>
##                                 <li><a href="">العربية</a></li>
##                                 <li><a href="">Русский</a></li>
##                             </ul>
                        </div>
                        <div class="text_copy_right">
                            تمامی حقوق برای اندروید بازار محفوظ است
                        </div>
                    </div>


                </footer>


<script src="/index/js/bootstrap.js"></script>
<script src="/index/js/main.js"></script>


</body>
</html>

