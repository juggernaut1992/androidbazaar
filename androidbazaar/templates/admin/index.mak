<%inherit file="local:templates.admin.master"/>

<%
    from khayyam import JalaliDatetime
    def translate_status(s):
        return {
            'queued': 'در صف انتظار',
            'edit_required': 'نیازمند تغییر',
            'stopped': 'توقف انتشار',
            'published': 'منتشر شده'
        }.get(s)
%>

<!-- row -->
<div class="row">
    <div class="col-md-12">
         <div class="white-box">
         <h3 class="box-title">فهرست برنامه ها</h3>
             <div class="scrollable">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام</th>
                                <th>توسعه دهنده</th>
                                <th>ایمیل</th>
                                <th>تلفن</th>
                                <th>وضعیت آخرین بسته</th>
                                <th>تاریخ ثبت</th>
                                <th>قیمت</th>
                                <th>دانلود APK</th>
                                <th>تغییر وضعیت آخرین بسته</th>
                            </tr>
                        </thead>
                        <tbody>
                            %for a in applications:
                                <%
                                    latest_release = a.latest_release()
                                %>
                                <tr>
                                    <td>${a.uid}</td>
                                    <td>${a.label}</td>
                                    <td>
                                        <a href="contact-detail.html">${a.tg_user.user_name}</a>
                                    </td>
                                    <td>${a.tg_user.email_address}</td>
                                    <td>0</td>
                                    <td><span class="label ${'label-success' if latest_release.status == 'published' else 'label-danger'}">${translate_status(latest_release.status)}</span> </td>
                                    <td>${JalaliDatetime(a.latest_release().created).strftime('%A %d %B %Y')}</td>
                                    <td>${a.price or 'رایگان'}</td>
                                    <td>
                                        <select class="releases" onchange="onValueChange(this);">
                                            %for r in a.releases:
                                                <option>انتخاب نسخه</option>
                                                <option value="http://185.208.175.54:8080/api/contents/${r.apk_uid}"><p> نسخه  ${r.version}</p></option>
                                            %endfor
                                        </select>
                                    </td>
                                    <td>
                                        <select class="statuses" onchange="onValueChange(this);">
                                            <option>تغییر وضعیت</option>
                                            <option value="${tg.url('/area51/toggle_release_status/{}/stopped'.format(a.uid))}">توقف انتشار</option>
                                            <option value="${tg.url('/area51/toggle_release_status/{}/published'.format(a.uid))}">انتشار</option>
                                        </select>
                                    </td>
                                </tr>
                            %endfor
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="text-right">
                                        <ul class="pagination"> </ul>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
             </div>
         </div>
    </div>
</div>
<!-- /row -->

<script>
    function onValueChange(elem) {
        window.location = elem.value;
    }
</script>