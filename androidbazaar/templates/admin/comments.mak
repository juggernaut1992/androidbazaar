<%inherit file="local:templates.admin.master"/>

<%
    from khayyam import JalaliDatetime
%>

<!-- row -->
<div class="row">
    <div class="col-md-12">
         <div class="white-box">
         <h3 class="box-title">فهرست نظرات</h3>
             <div class="scrollable">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>توسعه دهنده</th>
                                <th>ایمیل</th>
                                <th>تلفن</th>
                                <th>تاریخ ثبت</th>
                                <th>متن</th>
                                <th>امتیاز</th>
                                <th>وضعیت</th>
                            </tr>
                        </thead>
                        <tbody>
                            %for c in comments:
                                <tr>
                                    <td>${c.uid}</td>
                                    <td>
                                        <a href="contact-detail.html">${c.tg_user.user_name}</a>
                                    </td>
                                    <td>${c.tg_user.email_address}</td>
                                    <td>0</td>
                                    <td>${JalaliDatetime(a.latest_release().created).strftime('%A %d %B %Y')}</td>
                                    <td>${c.text}</td>
                                    <td>${c.rate} ستاره </td>
                                    <td><a href="${tg.url('/area51/toggle_comment_status/{}'.format(c.uid))}"></a></td>
                                </tr>
                            %endfor
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="text-right">
                                        <ul class="pagination"> </ul>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
             </div>
         </div>
    </div>
</div>
<!-- /row -->
