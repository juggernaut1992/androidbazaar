<%inherit file="local:templates.admin.master"/>

<%
    from khayyam import JalaliDatetime
%>

<!-- row -->
<div class="row">
    <div class="col-md-12">
         <div class="white-box">
         <h3 class="box-title">فهرست کاربران</h3>
             <div class="scrollable">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام</th>
                                <th>ایمیل</th>
                                <th>تلفن</th>
                                <th>تاریخ ثبت نام</th>
                                <th>اعتبار</th>
                            </tr>
                        </thead>
                        <tbody>
                            %for u in users:
                                <tr>
                                    <td>${u.uid}</td>
                                    <td>
                                        <a href="contact-detail.html">${u.user_name}</a>
                                    </td>
                                    <td>${u.email_address}</td>
                                    <td>0</td>
                                    <td>${JalaliDatetime(u.created).strftime('%A %d %B %Y')}</td>
                                    <td><a href="${tg.url('/area51/toggle_comment_status/{}'.format(u.uid))}"></a></td>
                                    <td>${u.balance} تومان </td>
                                </tr>
                            %endfor
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="text-right">
                                        <ul class="pagination"> </ul>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
             </div>
         </div>
    </div>
</div>
<!-- /row -->
