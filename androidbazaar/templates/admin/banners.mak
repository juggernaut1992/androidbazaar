<%inherit file="local:templates.admin.master"/>

<%
    from khayyam import JalaliDatetime
%>

<!-- row -->
<div class="row">
    <div class="col-md-12">
         <div class="white-box">
         <h3 class="box-title">فهرست بنرها</h3>
             <div class="scrollable">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>آدرس</th>
                                <th>دانلود تصویر</th>
                                <th>نوع بنر</th>
                            </tr>
                        </thead>
                        <tbody>
                            %for b in banners:
                                <tr>
                                    <td>${b.uid}</td>
                                    <td><a target="_blank" href="${b.url}">مشاهده</a></td>
                                    <td>
                                        <a target="_blank" href="${tg.url('/storage/{}'.format(b.img))}">دانلود</a>
                                    </td>
                                    <td>
                                        ${b.varying}
                                    </td>
                                </tr>
                            %endfor
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="text-right">
                                        <ul class="pagination"> </ul>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
             </div>
         </div>
    </div>
</div>
<!-- /row -->

<div class="row">
    <div class="col-sm-4">
        <div class="white-box">
        <form class="form-material form-horizontal" enctype="multipart/form-data" action="${tg.url('/area51/submit_banner')}" method="post">
            <div class="form-group">
                <label class="col-sm-12">ثبت بنر جدید</label>
                <div class="col-sm-12">
                    <select class="form-control" name="type" id="app-category-id">
                        <option value="banner">بنر (۳۶۰ عرض * ۲۰۸ طول)</option>
                        <option value="carousel">کاروسل (۳۵۰ عرض * ۲۰۲ طول)</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">آدرس بنر</label>
                <div class="col-sm-12">
                    <input name="url" type="url" class="form-control" placeholder="https://androidbazzar.com/...">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12">آپلود تصویر بنر</label>
                <div class="col-sm-12">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">انتخاب فایل</span> <span class="fileinput-exists">تغییر</span>
                        <input id="app-apk" type="file" name="img"> </span> <a href="form-material-elements.html#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">حذف</a> </div>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-sm-12">
                <button id="submitBtn" class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">ثبت بنر</button>
              </div>
            </div>
        </form>
    </div>
</div>
    </div>