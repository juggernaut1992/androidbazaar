<%inherit file="local:templates.admin.master"/>

<%
    from khayyam import JalaliDatetime
%>

<!-- row -->
<div class="row">
    <div class="col-md-12">
         <div class="white-box">
         <h3 class="box-title">درخواست های تسویه حساب</h3>
             <div class="scrollable">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام کاربری</th>
                                <th>موجودی فعلی کاربر</th>
                                <th>مبلغ</th>
                                <th>تاریخ ثبت</th>
                                <th>وضعیت</th>
                            </tr>
                        </thead>
                        <tbody>
                            %for w in withdraws:
                                <tr>
                                    <td>${w.uid}</td>
                                    <td>${w.tg_user.user_name}</td>
                                    <td>${w.tg_user.balance} تومان </td>
                                    <td>${w.amount} تومان </td>
                                    <td>${JalaliDatetime(w.created).strftime('%A %d %B %Y')}</td>
                                    <td>${'انجام شده' if w.is_done else 'در صف انجام'}</td>
                                    %if not w.is_done:
                                        <td><button class="btn btn-success" type="button" onclick="window.location='${tg.url('/area51/toggle_withdraw_status/{}/True'.format(w.uid))}'">تایید انجام</button></td>
                                    %else:
                                        <td><button class="btn btn-danger" type="button" onclick="window.location='${tg.url('/area51/toggle_withdraw_status/{}/False'.format(w.uid))}'">لغو انجام</button></td>
                                    %endif
                                </tr>
                            %endfor
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="text-right">
                                        <ul class="pagination"> </ul>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
             </div>
         </div>
    </div>
</div>
<!-- /row -->
