<%inherit file="local:templates.master"/>

<%
    from androidbazaar.model import Picture, DBSession

    carousels = DBSession.query(Picture).filter(Picture.varying == 'carousel').all()
    banners = DBSession.query(Picture).filter(Picture.varying == 'banner').all()
    def find_logo(application):
        logo = [t for t in application.thumbnails if t.variant == 'logo']
        logo_key = logo[0].key if logo else ''
        logo_filename = logo[0].filename if logo else ''
        return tg.url('/storage/{}/{}'.format(logo_key, logo_filename))

    def space_to_dash(s):
        return s.replace(' ', '-')
%>



<!-- Item slider-->
<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                <div class="carousel-inner">

                    %for c in carousels:
                        <div class="item ${'active' if carousels.index(c) == 0 else ''}">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <a href="${c.url}"><img src="${tg.url('/storage/{}'.format(c.img))}" class="img-responsive center-block"></a>
                            </div>
                        </div>
                    %endfor


                </div>

                <div id="slider-control">
                    <a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="/index/img/next.png"
                                                                                               alt="Left"
                                                                                               class="img-responsive"></a>
                    <a class="right carousel-control" href="#itemslider" data-slide="next"><img src="/index/img/prev.png"
                                                                                                alt="Right"
                                                                                                class="img-responsive"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Item slider end-->


<div class="container">
    <div class="header_box_products">

        <div class="col-md-9" style="padding-right: 0">
            <div class="heade_all_box1">
بهترین های برگزیده
            </div>
        </div>

        <div class="col-md-3">
                        <span class="more_cat">
                                            بیشتر
                            <i class="fa fa-arrow-left"></i>
                        </span>
        </div>

    </div>
    <div id="thumbnail-slider">
        <div class="inner">
            <ul>
               %for app in applications:

                <div class="box_products">
                    <li>
                        <a href="${tg.url('/applications/details/{}/{}/{}'.format(app.uid, app.package_name, space_to_dash(app.label)))}">

                            <img style="width: 150px;" src="${find_logo(app)}">

                            <div class="box_detail">
                                <p class="name_product">
                                    ${app.label}
                                </p>

                                <p class="price">
                                    رایگان
                                </p>

                                <p>

                                <div class="star_point">
                                    <div class="star-rating">
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="1">
                                    </div>
                                </div>
                                </p>
                            </div>
                        </a>
                    </li>
                </div>
                %endfor


            </ul>
        </div>
    </div>


    <div class="header_box_products">

        <div class="col-md-9" style="padding-right: 0">
            <div class="heade_all_box1">
                فروشگاه های مجازی
            </div>
##             <div class="heade_all_box2">
##                 Recent Launches
##             </div>
        </div>

        <div class="col-md-3">
			<span class="more_cat">
								بیشتر
				<i class="fa fa-arrow-left"></i>
			</span>
        </div>

    </div>
    <div id="thumbnail-slider2">
        <div class="inner">
            <ul>
                %for app in applications:

                <div class="box_products">
                    <li>
                        <a href="${tg.url('/applications/details/{}/{}/{}'.format(app.uid, app.package_name, space_to_dash(app.label)))}">

                            <img style="width: 150px;" src="${find_logo(app)}">

                            <div class="box_detail">
                                <p class="name_product">
                                    ${app.label}
                                </p>

                                <p class="price">
                                    رایگان
                                </p>

                                <p>

                                <div class="star_point">
                                    <div class="star-rating">
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="1">
                                    </div>
                                </div>
                                </p>
                            </div>
                        </a>
                    </li>
                </div>
                %endfor


            </ul>
        </div>
    </div>
</div>




    <div class="container">
        <div class="row">


            <div class="ads">
                %for b in banners:
                    <div class="col-md-4  col-xs-12">
                        <a href="${b.url}">
                            <img src="${b.img}" class="img-responsive" alt="">
                        </a>
                    </div>
                %endfor

            </div>
        </div>



    <div class="header_box_products">

        <div class="col-md-9" style="padding-right: 0">
            <div class="heade_all_box1">
                غذا و رستوران
            </div>
            <div class="heade_all_box2">
##                         Best New Apps
            </div>
        </div>

        <div class="col-md-3">
    <span class="more_cat">
                        بیشتر
        <i class="fa fa-arrow-left"></i>
    </span>
        </div>

    </div>
    <div id="thumbnail-slider3">
        <div class="inner">
            <ul>
                %for app in applications:

                <div class="box_products">
                    <li>
                        <a href="${tg.url('/applications/details/{}/{}/{}'.format(app.uid, app.package_name, space_to_dash(app.label)))}">

                            <img style="width: 150px;" src="${find_logo(app)}">

                            <div class="box_detail">
                                <p class="name_product">
                                    ${app.label}
                                </p>

                                <p class="price">
                                    رایگان
                                </p>

                                <p>

                                <div class="star_point">
                                    <div class="star-rating">
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="4">
                                    </div>
                                </div>
                                </p>
                            </div>
                        </a>
                    </li>
                </div>
                %endfor


            </ul>
        </div>
    </div>

</div>

<script src="/index/js/thumbnail-slider.js"></script>
<script>

var thumbnailSliderOptions =
{
    sliderId: "thumbnail-slider",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "200px",
    showMode: 1,
    autoAdvance: true,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};


var thumbnailSliderOptions2 =
{
    sliderId: "thumbnail-slider2",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "190px",
    showMode: 2,
    autoAdvance: false,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};


var thumbnailSliderOptions3 =
{
    sliderId: "thumbnail-slider3",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "190px",
    showMode: 2,
    autoAdvance: false,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};


var thumbnailSliderOptions4 =
{
    sliderId: "thumbnail-slider4",
    orientation: "horizontal",
    thumbWidth: "auto",
    thumbHeight: "190px",
    showMode: 2,
    autoAdvance: false,
    selectable: true,
    slideInterval: 3000,
    transitionSpeed: 1500,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 0,
    keyboardNav: true,
    mousewheelNav: false,
    before: null,
    license: "mylicense"
};





var thumbs2Op =
{
    sliderId: "thumbs2",
    orientation: "vertical",
    thumbWidth: "90px",
    thumbHeight: "auto",
    showMode: 3,
    autoAdvance: true,
    selectable: true,
    slideInterval: 2500,
    transitionSpeed: 800,
    shuffle: false,
    startSlideIndex: 0, //0-based
    pauseOnHover: true,
    initSliderByCallingInitFunc: false,
    rightGap: 100,
    keyboardNav: true,
    mousewheelNav: true,
    before: null,
    license: "mylicense"
};


var mcThumbnailSlider = new ThumbnailSlider(thumbnailSliderOptions);
var mcThumbnailSlider2 = new ThumbnailSlider(thumbnailSliderOptions2);
var mcThumbnailSlider3 = new ThumbnailSlider(thumbnailSliderOptions3);
var mcThumbnailSlider4 = new ThumbnailSlider(thumbnailSliderOptions4);
var mcThumbs2 = new ThumbnailSlider(thumbs2Op);
</script>