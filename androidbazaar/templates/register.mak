
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="${tg.url('/plugins/images/favicon.png')}">
<title>قالب مدیرخوب - قالب چند منظوره مدیریتی حرفه ای</title>
<!-- Bootstrap Core CSS -->
<link href="${tg.url('/plugins/bower_components/bootstrap-rtl-master/dist/css/bootstrap-rtl.min.css')}" rel="stylesheet">
<!-- animation CSS -->
<link href="${tg.url('/css/animate.css" rel="stylesheet')}">
<!-- Custom CSS -->
    <link href="${tg.url('/plugins/bower_components/toast-master/css/jquery.toast.css')}" rel="stylesheet">
<link href="${tg.url('/css/style.css')}" rel="stylesheet">
<!-- color CSS -->
<link href="${tg.url('/css/colors/blue.css')}" id="theme"  rel="stylesheet">
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" action="index.html">
        <a class="text-center" href="${tg.url('/')}"> لوگو اندروید بازار</a>
        <h3 class="box-title m-t-40 m-b-0">ثبت نام کنید</h3><small>حساب خود را ایجاد کنید</small>
        <div class="form-group m-t-20">
          <div class="col-xs-12">
            <input class="form-control" type="text" id="username" required="" placeholder="نام کاربری">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="email" id="email" required="" placeholder="ایمیل">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" id="password" required="" placeholder="رمز">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" id="confirm" required="" placeholder="تکرار رمز">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> من با همه قوانین موافقم <a href="register2.html#">شرایط و ضوابط</a></label>
            </div>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button id="registerBtn" onclick="register_($('#username').val(), $('#email').val(), $('#password').val(), $('#confirm').val())" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="button">ثبت نام</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>در حال حاضر یک حساب کاربری دارید؟ <a href="${tg.url('/login')}" class="text-primary m-l-5"><b>ورود</b></a></p>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="${tg.url('/plugins/bower_components/jquery/dist/jquery.min.js')}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${tg.url('/plugins/bower_components/bootstrap-rtl-master/dist/js/bootstrap-rtl.min.js')}"></script>
<!-- Menu Plugin JavaScript -->
<script src="${tg.url('/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}"></script>

<!--slimscroll JavaScript -->
<script src="${tg.url('/js/jquery.slimscroll.js')}"></script>
<!--Wave Effects -->
<script src="${tg.url('/js/waves.js')}"></script>
<!-- Custom Theme JavaScript -->
<script src="${tg.url('/js/custom.js')}"></script>
<!--Style Switcher -->
<script src="${tg.url('/plugins/bower_components/toast-master/js/jquery.toast.js')}"></script>
<script src="${tg.url('/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}"></script>
</body>
</html>
<script>
    var registerBtn  = $('#registerBtn');
    function register_(username, email, password, confirm) {
        if (password !== confirm) {
            alertify('گذرواژه و تکرار آن مطابقت ندارند');
            return
        }
        var formData = JSON.stringify({user_name: username, password: password, email_address: email});
        registerBtn.prop('disabled', 'disabled');
        $.ajax({
               url : '${base_url}' + '/api/users/',
               type : 'POST',
               data : formData,
               processData: false,
               contentType: 'application/json',
               success : function(resp) {
                   window.location = '${base_url}' + '/login'
               },
                error: function(resp) {
                   registerBtn.removeAttr('disabled');
                   registerBtn.text('ثبت');
                   alertify(resp.responseJSON.detail)
            }
        });
}
</script>