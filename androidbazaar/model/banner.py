from sqlalchemy.types import Integer, Unicode, Enum
from tg import abort

from androidbazaar.model import DeclarativeBase, DBSession
from androidbazaar.lib.mixins import ConstructorMixin
from androidbazaar.lib.pillar import Pillar
from androidbazaar.lib.storage import Attachment


class Picture(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'pictures'

    uid = Pillar(Integer, primary_key=True)
    img = Pillar(Unicode, nullable=False, unique=True)
    url = Pillar(Unicode, nullable=False)
    varying = Pillar(Enum('carousel', 'banner', name='picture_types'), nullable=False)

    @classmethod
    def update_from_request(cls, instance=None, kwargs=None):
        data = cls._extract_json_from_request() or kwargs
        if not data:
            abort(400, detail='No data in request!', passthrough='json')

        instance = cls()
        instance.url = kwargs.get('url')
        instance.varying = kwargs.get('type')
        img = Attachment.from_field_storage(data.get('img'))
        instance.img = '{}/{}'.format(img.folder_name, img.filename)

        DBSession.add(instance)
        DBSession.flush()
