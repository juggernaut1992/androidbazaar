from sqlalchemy.types import Integer, Unicode
from sqlalchemy.orm import relationship, backref

from androidbazaar.model import DeclarativeBase
from androidbazaar.lib.mixins import ConstructorMixin
from androidbazaar.lib.pillar import Pillar


class Category(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'categories'

    uid = Pillar(Integer, primary_key=True)
    name = Pillar(Unicode, nullable=False, unique=True)

    applications = relationship('Application', backref=backref('categories'), cascade="all, delete-orphan")
