# -*- coding: utf-8 -*-
import os
from datetime import datetime
from hashlib import sha256

from sqlalchemy import Table, ForeignKey
from sqlalchemy.exc import IntegrityError
from tg import request, abort, config
from sqlalchemy.types import Unicode, Integer, DateTime, Boolean
from sqlalchemy.orm import relation, synonym, relationship, backref
from redis import StrictRedis

from androidbazaar.model import metadata, DBSession, DeclarativeBase
from androidbazaar import model
from androidbazaar.lib.mixins import ConstructorMixin
from androidbazaar.lib.pillar import Pillar
from androidbazaar.lib.helpers import EMAIL_PATTERN, random_string
from androidbazaar.lib.storage import IDCardAttachment


group_permission_table = Table('tg_group_permission', metadata,
                               Pillar('group_id', Integer,
                                      ForeignKey('tg_group.group_id',
                                                 onupdate="CASCADE",
                                                 ondelete="CASCADE"),
                                      primary_key=True),
                               Pillar('permission_id', Integer,
                                      ForeignKey('tg_permission.permission_id',
                                                 onupdate="CASCADE",
                                                 ondelete="CASCADE"),
                                      primary_key=True))


user_group_table = Table('tg_user_group', metadata,
                         Pillar('user_id', Integer,
                                ForeignKey('tg_user.uid',
                                           onupdate="CASCADE",
                                           ondelete="CASCADE"),
                                primary_key=True),
                         Pillar('group_id', Integer,
                                ForeignKey('tg_group.group_id',
                                           onupdate="CASCADE",
                                           ondelete="CASCADE"),
                                primary_key=True))


class Group(DeclarativeBase):
    __tablename__ = 'tg_group'

    group_id = Pillar(Integer, autoincrement=True, primary_key=True)
    group_name = Pillar(Unicode(16), unique=True, nullable=False)
    display_name = Pillar(Unicode(255))
    created = Pillar(DateTime, default=datetime.now)
    users = relation('User', secondary=user_group_table, backref='groups')

    def __repr__(self):
        return '<Group: name=%s>' % repr(self.group_name)

    def __unicode__(self):
        return self.group_name


class User(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'tg_user'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    user_name = Pillar(Unicode(16), unique=True, nullable=False, min_length=4, max_length=12, modifiable=True)
    email_address = Pillar(Unicode(255), unique=True, nullable=False, pattern=EMAIL_PATTERN, modifiable=True)
    _password = Pillar('password', Unicode(128), modifiable=True)
    created = Pillar(DateTime, default=datetime.now)
    is_dev = Pillar(Boolean, default=True)
    balance = Pillar(Integer, default=0)

    comments = relationship('Comment', backref=backref('tg_user'), cascade="all, delete-orphan")
    applications = relationship('Application', backref=backref('tg_user'), cascade="all, delete-orphan")
    reactions = relationship('CommentReaction', backref=backref('tg_user'), cascade="all, delete-orphan")
    reports = relationship('MalwareReport', backref=backref('tg_user'), cascade="all, delete-orphan")
    views = relationship('ApplicationView', backref=backref('tg_user'), cascade="all, delete-orphan")
    purchases = relationship('Purchase', backref=backref('tg_user'), cascade="all, delete-orphan")
    withdraws = relationship('Withdraw', backref=backref('tg_user'), cascade="all, delete-orphan")

    def __repr__(self):
        return '<User: name=%s, email=%s, display=%s>' % (
            repr(self.user_name),
            repr(self.email_address),
            repr(self.display_name)
        )

    def __unicode__(self):
        return self.display_name or self.user_name

    @property
    def permissions(self):
        perms = set()
        for g in self.groups:
            perms = perms | set(g.permissions)
        return perms

    @classmethod
    def _hash_password(cls, password):
        salt = sha256()
        salt.update(os.urandom(60))
        salt = salt.hexdigest()

        hash = sha256()
        # Make sure password is a str because we cannot hash unicode objects
        hash.update((password + salt).encode('utf-8'))
        hash = hash.hexdigest()

        password = salt + hash

        return password

    def _set_password(self, password):
        self._password = self._hash_password(password)

    def _get_password(self):
        return self._password

    password = synonym('_password', descriptor=property(_get_password,
                                                        _set_password))

    def validate_password(self, password):
        hash = sha256()
        hash.update((password + self.password[:64]).encode('utf-8'))
        return self.password[64:] == hash.hexdigest()

    def buy(self, app):
        if self.balance < app.price:
            abort(403, detail='Insufficient balance', passthrough='json')
        self.purchases.append(
            model.Purchase(application_id=app.uid, value=app.price),
        )
        try:
            DBSession.flush()
        except IntegrityError as ex:
            DBSession.rollback()
            abort(409, detail='You\'ve bought this before!', passthrough='json')

    def has_bought(self, app):
        return True if app.uid in [p.application_id for p in self.purchases] else False

    @classmethod
    def current(cls):
        identity = request.identity
        if not identity:
            return None
        return identity['user']

    def request_withdraw(self, kwargs):
        amount = int(kwargs.get('amount'))
        if amount < 1000 or amount > self.balance:
            abort(400, detail='Double check the amount', passthrough='json')
        self.withdraws.append(
            model.Withdraw.from_attachment(
                user_id=self.uid,
                id_card_attachment=IDCardAttachment.from_field_storage(kwargs.get('image')),
                name=kwargs.get('holderName'),
                card=kwargs.get('holderCartNumber'),
                description=kwargs.get('description'),
                amount=amount
            )
        )
        DBSession.flush()

    @classmethod
    def create_session_and_return(cls):
        """For using in the app"""
        payload = cls._extract_json_from_request()
        user = DBSession.query(User).filter(User.user_name == payload.get('user_name')).one_or_none()
        if not user:
            abort(404, passthrough='json')
        if user.validate_password(payload.get('password')):
            session = random_string(256)
            redis = StrictRedis(db=config.get('redis_db'))
            redis.set(user.uid, session)
            return user, session
        else:
            abort(401, passthrough='json')

    @classmethod
    def from_session(cls):
        user_id = int(request.environ.get('HTTP_USER'))
        session_id = request.environ.get('HTTP_SESSION')
        if not user_id or not session_id:
            abort(401, passthrough='json')

        conn = StrictRedis(db=config.get('redis_session_db'))
        stored_session = conn.get(user_id)
        if not stored_session:
            abort(401, passthrough='json')
        if stored_session.decode() != session_id:
            abort(401, passthrough='json')

        return cls.one_or_none(user_id)


class Permission(DeclarativeBase):
    __tablename__ = 'tg_permission'

    permission_id = Pillar(Integer, autoincrement=True, primary_key=True)
    permission_name = Pillar(Unicode(63), unique=True, nullable=False)
    description = Pillar(Unicode(255))

    groups = relation(Group, secondary=group_permission_table,
                      backref='permissions')

    def __repr__(self):
        return '<Permission: name=%s>' % repr(self.permission_name)

    def __unicode__(self):
        return self.permission_name
