from shutil import rmtree
from datetime import datetime
from os import path
from packaging import version

from sqlalchemy.types import Integer, Unicode, Enum, Boolean, DateTime, Float
from sqlalchemy import ForeignKey
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.schema import UniqueConstraint
from tg import abort, config

from androidbazaar.model import DeclarativeBase, DBSession, User
from androidbazaar.lib.pillar import Pillar
from androidbazaar.lib.mixins import ConstructorMixin
from androidbazaar.lib.parser import APKParser
from androidbazaar.lib.buzzle_client import BuzzleClient
from androidbazaar.lib.storage import ThumbnailAttachment
from androidbazaar.lib.helpers import human_readable_size
from androidbazaar.lib.html_to_text import dehtml


class Application(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'applications'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)

    label = Pillar(Unicode(50), nullable=False, min_length=3, max_length=50, modifiable=True)
    package_name = Pillar(Unicode(100), unique=True, min_length=9, max_length=100, modifiable=False)
    fa_description = Pillar(Unicode, nullable=True, modifiable=True)
    en_description = Pillar(Unicode, nullable=True, modifiable=True)
    price = Pillar(Integer, default=0, modifiable=True)

    category_id = Pillar(
        Integer,
        ForeignKey('categories.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False,
        modifiable=True
    )

    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )

    releases = relationship('Release', backref=backref('applications'), cascade="all, delete-orphan")
    installs = relationship('Install', backref=backref('applications'), cascade="all, delete-orphan")
    comments = relationship('Comment', backref=backref('applications'), cascade="all, delete-orphan")
    reports = relationship('MalwareReport', backref=backref('applications'), cascade="all, delete-orphan")
    thumbnails = relationship('Thumbnail', backref=backref('applications'), cascade="all, delete-orphan")
    views = relationship('ApplicationView', backref=backref('applications'), cascade="all, delete-orphan")

    @classmethod
    def update_from_request(cls, instance=None, kwargs=None):
        data = cls._extract_json_from_request() or kwargs
        if not data:
            abort(400, detail='No data in request!', passthrough='json')

        cat_id = data.get('cat_id')

        if not instance:
            instance = cls()

            apk = data.get('apk')

            parser = APKParser(apk)
            package = parser.parse()
            if package.debug:
                abort(400, detail='حالت debug فعال است!', passthrough='json')

            instance.label = package.label
            instance.package_name = package.name

            if cls.by_package_name(instance.package_name):
                abort(409, detail='Package name already exists!', passthrough='json')

            current_release = Release()
            current_release.version = package.version
            current_release.sdkVersion = package.sdkVersion
            current_release.sha1 = package.sha1
            current_release.validity_date = package.validity_date
            current_release.owner = package.owner
            current_release.permissions = package.permissions

            buzzle = BuzzleClient(apk.filename, parser.file_path)
            current_release.apk_uid = buzzle.store()
            parser.cleanup()
            current_release.size = human_readable_size(data.get('apk').bytes_read)

            instance.releases.append(current_release)

        elif instance:
            instance.label = data.get('label')
            instance.price = data.get('price')
            instance.fa_description = data.get('fa_description')
            instance.en_description = data.get('en_description')

        instance.category_id = cat_id
        instance.user_id = User.current().uid

        DBSession.add(instance)

        DBSession.flush()

        return instance

    def attach_thumbnail(self, field, variant):
        if variant == 'logo':
            logo = [t for t in self.thumbnails if t.variant == 'logo']
            if logo:
                logo[0].delete()
        self.thumbnails.append(
            Thumbnail.from_attachment(self.uid, ThumbnailAttachment.from_field_storage(field), variant)
        )
        DBSession.flush()

    @classmethod
    def by_package_name(cls, package_name):
        return DBSession.query(Application).filter(Application.package_name == package_name).one_or_none()

    def latest_release(self):
        return DBSession.query(Release).filter(Release.application_id == self.uid).order_by(Release.uid.desc()).first()

    def up_view(self, user):
        if DBSession.\
            query(ApplicationView).\
                filter(ApplicationView.user_id == user.uid). \
                filter(ApplicationView.application_id == self.uid). \
                one_or_none():
            return
        DBSession.add(ApplicationView(user_id=user.uid, application_id=self.uid))
        DBSession.flush()

    def new_release_from_request(self, **kwargs):

        lr = self.latest_release()
        en_log = kwargs.get('en_log')
        fa_log = kwargs.get('fa_log')
        apk = kwargs.get('apk')

        parser = APKParser(apk)
        package = parser.parse()

        if package.name != self.package_name:
            abort(400, detail='نام بسته باید با نام بسته ی نسخه های قبلی یکی باشد', passthrough='json')

        if version.parse(lr.version) >= version.parse(package.version):
            abort(400, detail='ورژن کد را افزایش دهید', passthrough='json')

        if package.debug:
            abort(400, detail='حالت debug فعال است', passthrough='json')

        if lr.sha1 != package.sha1 or lr.owner != package.owner or lr.validity_date != package.validity_date:
            abort(400, detail='certificate check failed!', passthrough='json')

        new = Release()
        buzzle = BuzzleClient(apk.filename, parser.file_path)
        new.apk_uid = buzzle.store()
        new.fa_log = fa_log
        new.en_log = en_log
        new.version = package.version
        new.permissions = package.permissions
        new.size = human_readable_size(apk.bytes_read)

        self.releases.append(new)
        DBSession.flush()

    def similar_apps(self):
        return DBSession.\
            query(Application).\
            filter(Application.category_id == self.category_id). \
            filter(Application.label.contains(self.label)). \
            all()

    @classmethod
    def top_apps(cls):
        _apps = []
        apps = DBSession.query(Application).all()
        for a in apps:
            logo = [t for t in a.thumbnails if t.variant == 'logo']
            logo_key = logo[0].key if logo else ''
            logo_filename = logo[0].filename if logo else ''
            app = {
                'label': a.label,
                'price': a.price,
                'uid': a.uid,
                'thumbnail': '/storage/{}/{}'.format(logo_key, logo_filename)
            }
            _apps.append(app)
        return _apps

    def in_details(self):
        latest_release = self.latest_release()
        _all = self.thumbnails

        return {
            'uid': self.uid,
            'label': self.label,
            'description': dehtml(self.fa_description),
            'price': self.price,
            'rating': 5,
            'author': self.tg_user.user_name,
            'logo': [x for x in _all if x.variant == 'logo'][0],
            'thumbnails': [x for x in _all if x.variant == 'thumbnail'],
            'permissions': latest_release.permissions.split(','),
            'sdkVersion': latest_release.sdkVersion,
            'size': latest_release.size,
            'changelog': latest_release.fa_log,
            'installs': len(self.installs)
        }


class Comment(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'comments'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    text = Pillar(Unicode(255), nullable=False, min_length=5, max_length=100)
    rate = Pillar(Enum('1', '2', '3', '4', '5', name='app_rate_enum'), nullable=False)
    approved = Pillar(Boolean, default=False)

    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    application_id = Pillar(
        Integer,
        ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    reactions = relationship('CommentReaction', backref=backref('comments'), cascade="all, delete-orphan")


class CommentReaction(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'comment_reactions'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    reaction = Pillar(Enum('like', 'dislike', name='reaction_enum'), nullable=False)

    comment_id = Pillar(
        Integer,
        ForeignKey('comments.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )

    __table_args__ = (UniqueConstraint('user_id', 'comment_id'), )


class Release(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'releases'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)

    version = Pillar(Unicode, nullable=False)
    sdkVersion = Pillar(Unicode, nullable=False)
    permissions = Pillar(Unicode, nullable=False)
    apk_uid = Pillar(Unicode, nullable=False)  # from buzzle
    en_log = Pillar(Unicode, nullable=True, modifiable=True)
    fa_log = Pillar(Unicode, nullable=True, modifiable=True)
    size = Pillar(Unicode, nullable=False)
    # Special Zone
    sha1 = Pillar(Unicode, nullable=False)
    validity_date = Pillar(Unicode, nullable=False)
    owner = Pillar(Unicode, nullable=False)
    # /Special Zone
    created = Pillar(DateTime, default=datetime.now)
    status = Pillar(
        Enum('queued', 'edit_required', 'stopped', 'published', name='app_statuses'),
        default='queued'
    )

    application_id = Pillar(
        Integer,
        ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )

    @classmethod
    def from_apk_uid(cls, apk_uid):
        return DBSession.query(Release).filter(Release.apk_uid == apk_uid).one_or_none()


class MalwareReport(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'malware_reports'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    text = Pillar(Unicode(255), nullable=False)

    application_id = Pillar(
        Integer,
        ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    __table_args__ = (UniqueConstraint('user_id', 'application_id'), )


class Thumbnail(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'attachments'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    key = Pillar(Unicode, unique=True, nullable=False)
    filename = Pillar(Unicode, nullable=False)
    variant = Pillar(Enum('thumbnail', 'logo', name='thumbnail_variants'), nullable=False)

    application_id = Pillar(
        Integer,
        ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )

    @classmethod
    def from_attachment(cls, app_uid, thumbnail_attachment: ThumbnailAttachment, variant):
        instance = cls()
        instance.key = thumbnail_attachment.folder_name
        instance.filename = thumbnail_attachment.filename
        instance.application_id = app_uid
        instance.variant = variant

        return instance

    def _purge(self):
        storage_path = path.join(config['paths']['static_files'], 'storage')
        rmtree(path.join(storage_path, self.key))

    def delete(self):
        self._purge()
        DBSession.delete(self)
        DBSession.flush()


class ApplicationView(DeclarativeBase):
    __tablename__ = 'app_views'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    application_id = Pillar(
        Integer,
        ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    created = Pillar(DateTime, default=datetime.now)
    __table_args__ = (UniqueConstraint('user_id', 'application_id'),)


class Purchase(DeclarativeBase):
    __tablename__ = 'purchases'

    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    application_id = Pillar(
            Integer,
            ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
            nullable=False
        )
    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    value = Pillar(Integer, nullable=False)
    created = Pillar(DateTime, default=datetime.now)
    __table_args__ = (UniqueConstraint('user_id', 'application_id'),)


class Install(DeclarativeBase):
    __tablename__ = 'installs'
    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    application_id = Pillar(
            Integer,
            ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
            nullable=False
        )
    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    created = Pillar(DateTime, default=datetime.now)
    __table_args__ = (UniqueConstraint('user_id', 'application_id'),)


class UnInstall(DeclarativeBase):
    __tablename__ = 'uninstalls'
    uid = Pillar(Integer, autoincrement=True, primary_key=True)
    application_id = Pillar(
            Integer,
            ForeignKey('applications.uid', onupdate='CASCADE', ondelete='CASCADE'),
            nullable=False
        )
    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )
    created = Pillar(DateTime, default=datetime.now)
    __table_args__ = (UniqueConstraint('user_id', 'application_id'),)
