# -*- coding: utf-8 -*-
from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.types import Unicode, Integer, Boolean, DateTime

from androidbazaar.model import DeclarativeBase
from androidbazaar.lib.mixins import ConstructorMixin
from androidbazaar.lib.pillar import Pillar


class Withdraw(DeclarativeBase, ConstructorMixin):
    __tablename__ = 'withdraws'

    uid = Pillar(Integer, primary_key=True, autoincrement=True)

    name = Pillar(Unicode, nullable=False)
    description = Pillar(Unicode, nullable=True)
    card = Pillar(Unicode, nullable=False)
    amount = Pillar(Integer, nullable=False)
    key = Pillar(Unicode, unique=True, nullable=False)
    filename = Pillar(Unicode, nullable=False)
    is_done = Pillar(Boolean, default=False)
    created = Pillar(DateTime, default=datetime.now)

    user_id = Pillar(
        Integer,
        ForeignKey('tg_user.uid', onupdate='CASCADE', ondelete='CASCADE'),
        nullable=False
    )

    @classmethod
    def from_attachment(cls, user_id, id_card_attachment, name, card, description, amount):
        instance = cls()

        instance.user_id = user_id
        instance.key = id_card_attachment.folder_name
        instance.filename = id_card_attachment.filename
        instance.name = name
        instance.card = card
        instance.description = description
        instance.amount = amount

        return instance
