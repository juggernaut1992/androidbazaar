# -*- coding: utf-8 -*-
"""Setup the androidbazaar application"""
from __future__ import print_function, unicode_literals
import transaction
from androidbazaar import model


def bootstrap(command, conf, vars):
    """Place any commands to setup androidbazaar here"""

    categories = [
        'ورزشی',
        'مالی',
        'مذهبی',
        'کسب و کار',
        'کتاب',
        'کاربردی',
        'عکس و فیلم',
        'صوت و موسیقی',
        'سبک زندگی',
        'سرگرمی',
        'سیر و سفر',
        'شخصی سازی',
        'آموزشی',
        'پزشکی و سلامت',
        'تندرستی و تناسب اندام',
        'خرید',
        'بازی',
        'آب و هوا',
        'اجتماعی',
        'اخبار',
        'آشپزی و رستوران'
    ]

    # <websetup.bootstrap.before.auth
    from sqlalchemy.exc import IntegrityError
    try:
        u = model.User()
        u.user_name = 'manager'
        u.display_name = 'Example manager'
        u.email_address = 'manager@somedomain.com'
        u.password = 'managepass'
        u.is_dev = True

        model.DBSession.add(u)

        for c in categories:
            model.DBSession.add(
                model.Category(
                    name=c
                )
            )

        g = model.Group()
        g.group_name = 'managers'
        g.display_name = 'Managers Group'

        g.users.append(u)

        model.DBSession.add(g)

        p = model.Permission()
        p.permission_name = 'manage'
        p.description = 'This permission gives an administrative right'
        p.groups.append(g)

        model.DBSession.add(p)

        model.DBSession.flush()
        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your auth data, '
              'it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    # <websetup.bootstrap.after.auth>
