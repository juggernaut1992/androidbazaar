from tg import expose, predicates, redirect

from androidbazaar.lib.base import BaseController
from androidbazaar.model import DBSession, Application, Comment, User, Withdraw, Picture


class AndroidBazaarAdminController(BaseController):
    allow_only = predicates.has_permission('manage')

    @expose('androidbazaar.templates.admin.index')
    def index(self):
        return dict(applications=Application.all())

    @expose('androidbazaar.templates.admin.comments')
    def comments(self):
        return dict(comments=Comment.all())

    @expose('androidbazaar.templates.admin.banners')
    def banners(self):
        return dict(banners=Picture.all())

    @expose('androidbazaar.templates.admin.withdraws')
    def withdraws(self):
        return dict(withdraws=Withdraw.all())

    @expose('androidbazaar.templates.admin.users')
    def users(self):
        return dict(users=User.all())

    @expose()
    def toggle_release_status(self, app_uid, status):
        latest_release = Application.one_or_none(app_uid).latest_release()
        latest_release.status = status
        DBSession.flush()
        redirect('/area51/')

    @expose()
    def toggle_comment_status(self, c_uid):
        comment = Comment.one_or_none(c_uid)
        comment.status = True if comment.status is False else False
        DBSession.flush()
        redirect('/area51/comments')

    @expose()
    def toggle_withdraw_status(self, w_uid, status):
        withdraw = Withdraw.one_or_none(w_uid)
        withdraw.is_done = True if status == 'True' else False
        DBSession.flush()
        redirect('/area51/withdraws')

    @expose()
    def submit_banner(self, **kwargs):
        Picture.update_from_request(kwargs=kwargs)
        redirect('/area51/banners')

    # @expose()
    # def toggle_user_status(self, w_uid, status):
    #     withdraw = Withdraw.one_or_none(w_uid)
    #     withdraw.is_done = True if status == 'True' else False
    #     DBSession.flush()
    #     redirect('/area51/withdraws')
