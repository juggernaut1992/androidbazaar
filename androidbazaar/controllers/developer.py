from tg import expose, abort

from androidbazaar.lib.base import BaseController
from androidbazaar.model import User, Application, DBSession, Category, Release, Comment


class DeveloperController(BaseController):

    @expose('androidbazaar.templates.developer.index')
    def applications(self):
        user = User.current()
        return dict(user=user, applications=user.applications)

    @expose('androidbazaar.templates.developer.withdraw')
    def withdraw(self):
        return dict(user=User.current())

    @expose('androidbazaar.templates.developer.withdraws')
    def withdraws(self):
        return dict(withdraws=User.current().withdraws)

    @expose('androidbazaar.templates.developer.sale')
    def sale(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        return dict(application=application)

    @expose('androidbazaar.templates.developer.application')
    def application(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        return dict(application=application, categories=DBSession.query(Category).all())

    @expose('androidbazaar.templates.developer.new')
    def new(self):
        return dict(categories=DBSession.query(Category).all())

    @expose('androidbazaar.templates.developer.new_release')
    def release(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        return dict(application=application)

    @expose('androidbazaar.templates.developer.thumbnail')
    def thumbnails(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        return dict(thumbnails=application.thumbnails, app_title=application.label, app_uid=app_uid)

    @expose('androidbazaar.templates.developer.release')
    def releases(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        return dict(
            releases=application.releases,
            app_title=application.label,
            app_uid=app_uid,
            package_name=application.package_name
        )

    @expose('androidbazaar.templates.developer.changelog')
    def changelog(self, release_uid, package_name, app_title):
        release = Release.one_or_none(release_uid)

        if not release:
            abort(404)

        if release.applications.user_id != User.current().uid:
            abort(403)

        return dict(
            release=release,
            app_title=release.applications.label,
            app_uid=release.applications.uid,
            package_name=package_name
        )

    @expose('androidbazaar.templates.developer.statistics')
    def statistics(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        return dict(application=application)

    @expose('androidbazaar.templates.developer.comments')
    def comments(self, app_uid, package_name, app_title, page_number):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        comments = DBSession.\
            query(Comment).\
            order_by(Comment.uid.desc()).\
            limit(15).\
            offset(page_number * 15).\
            all()
        return dict(application=application, comments=comments)
