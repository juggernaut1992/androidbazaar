from datetime import datetime, timedelta

from tg.controllers.restcontroller import RestController
from tg import expose, abort
from sqlalchemy import cast, Date, func

from androidbazaar.model import Application, User, DBSession, Purchase, ApplicationView, Install, UnInstall


class ReportController(RestController):

    @expose('json')
    def sale(self, app_uid):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        purchases = DBSession. \
            query(Purchase.created, func.sum(Purchase.value.label('total'))). \
            filter(Purchase.application_id == app_uid). \
            filter(cast(Purchase.created, Date) >= datetime.today() - timedelta(days=21)). \
            order_by(Purchase.created). \
            group_by(Purchase.created). \
            all()
        data = {}
        for p in purchases:
            key = p[0].strftime('%d %B')
            if key not in data.keys():
                data[key] = p[1]
            else:
                data[key] += p[1]

        return dict(data=data)

    @expose('json')
    def view(self, app_uid):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        views = DBSession. \
            query(ApplicationView.created). \
            filter(ApplicationView.application_id == app_uid). \
            filter(cast(ApplicationView.created, Date) >= datetime.today() - timedelta(days=21)). \
            order_by(ApplicationView.created). \
            all()
        data = {}
        for v in views:
            key = v.created.strftime('%d %B')
            if key not in data.keys():
                data[key] = 1
            else:
                data[key] += 1

        return dict(data=data)

    @expose('json')
    def installs(self, app_uid):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404)

        if application.user_id != User.current().uid:
            abort(403)

        installs = DBSession. \
            query(Install.created). \
            filter(Install.application_id == app_uid). \
            filter(cast(Install.created, Date) >= datetime.today() - timedelta(days=21)). \
            order_by(Install.created). \
            all()
        installs_data = {}
        for i in installs:
            key = i.created.strftime('%d %B')
            if key not in installs_data.keys():
                installs_data[key] = 1
            else:
                installs_data[key] += 1

        uninstalls = DBSession. \
            query(UnInstall.created). \
            filter(UnInstall.application_id == app_uid). \
            filter(cast(UnInstall.created, Date) >= datetime.today() - timedelta(days=21)). \
            order_by(UnInstall.created). \
            all()
        uninstalls_data = {}
        for un in uninstalls:
            key = un.created.strftime('%d %B')
            if key not in uninstalls_data.keys():
                uninstalls_data[key] = 1
            else:
                uninstalls_data[key] += 1

        return dict(installs_data=installs_data, uninstalls_data=uninstalls_data)

    @expose('json')
    def sales(self):
        report = DBSession. \
            query(Purchase.created, func.sum(Purchase.value.label('total'))). \
            join(Purchase.applications). \
            filter(Purchase.applications.user_id == User.current().uid). \
            group_by(Purchase.created). \
            all()
        return dict(report=report)
