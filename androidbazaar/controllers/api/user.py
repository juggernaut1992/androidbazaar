from tg.controllers.restcontroller import RestController
from tg import expose, abort

from androidbazaar.model.auth import User


class UserController(RestController):

    @expose('json')
    def post(self):
        return dict(user_id=User.update_from_request().uid)

    @expose('json')
    def get(self, uid):
        user = User.one_or_none(uid)
        if not user:
            abort(404, passthrough='json')
        return dict(user=user)

    @expose('json')
    def put(self):
        raise NotImplementedError

    @expose('json')
    def delete(self):
        raise NotImplementedError

    @expose('json')
    def request_session(self):
        user, session = User.create_session_and_return()
        return dict(session=session, user=user)
