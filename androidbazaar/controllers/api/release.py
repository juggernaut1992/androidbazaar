from tg.controllers.restcontroller import RestController
from tg import expose, abort

from androidbazaar.model.application import Application, Release
from androidbazaar.model.auth import User
from androidbazaar.lib.decorators import authorize


class ReleaseController(RestController):

    @expose('json')
    def post(self, app_uid, **kwargs):
        application = Application.one_or_none(app_uid)

        if not application:
            abort(404, passthrough='json')

        if application.user_id != User.current().uid:
            abort(403, passthrough='json')

        application.new_release_from_request(**kwargs)

        return dict(ok=True)

    @expose('json')
    def get(self, uid):
        raise NotImplementedError

    @expose('json')
    def put(self, release_uid, **kwargs):
        release = Release.one_or_none(release_uid)

        if not release:
            abort(404, passthrough='json')

        if release.applications.user_id != User.current().uid:
            abort(403, passthrough='json')

        release.update_from_request(instance=release, kwargs=kwargs)

        return dict(ok=True)

    @expose('json')
    def delete(self):
        raise NotImplementedError
