from tg.controllers.restcontroller import RestController
from tg import expose, abort

from androidbazaar.model.application import Application, Thumbnail
from androidbazaar.model.auth import User


class ThumbnailController(RestController):

    @expose('json')
    def post(self, **kwargs):
        uid = kwargs.get('uid')
        del kwargs['uid']
        application = Application.one_or_none(uid)
        if not application:
            abort(404, passthrough='json')

        if application.user_id != User.current().uid:
            abort(403, passthrough='json')

        application.attach_thumbnail(kwargs.get('image'), kwargs.get('variant'))
        return dict(ok=True)

    @expose('json')
    def get(self, uid):
        thumbnail = Thumbnail.one_or_none(uid)
        if not thumbnail:
            abort(404, passthrough='json')

        return dict(thumbnail=thumbnail)

    @expose('json')
    def put(self):
        raise NotImplementedError

    @expose('json')
    def delete(self, uid):
        thumbnail = Thumbnail.one_or_none(uid)

        if not thumbnail:
            abort(404, passthrough='json')

        if thumbnail.applications.user_id != User.current().uid:
            abort(403, passthrough='json')

        thumbnail.delete()
        return dict(ok=True)
