from tg.controllers.restcontroller import RestController
from tg import expose

from androidbazaar.model import User


class WithdrawController(RestController):

    @expose('json')
    def post(self, **kwargs):
        User.current().request_withdraw(kwargs)
        return dict(ok=True)

    @expose('json')
    def get(self, uid):
        raise NotImplementedError

    @expose('json')
    def put(self):
        raise NotImplementedError

    @expose('json')
    def delete(self):
        raise NotImplementedError
