from tg.controllers.restcontroller import RestController
from tg import expose, abort

from androidbazaar.model.application import Application, User
from androidbazaar.lib.decorators import authorize


class PurchaseController(RestController):

    @expose('json')
    @authorize()
    def post(self, app_id, **kwargs):
        user = kwargs.get('user')

        app = Application.one_or_none(app_id)
        if not app:
            abort(400, passthrough='json')

        user.buy(app)
        return dict(ok=True)
        
    @expose('json')
    def put(self, uid, **kwargs):
        raise NotImplementedError

    @expose('json')
    def delete(self):
        raise NotImplementedError
