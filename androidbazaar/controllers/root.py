# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import request, redirect, tmpl_context, expose, flash, abort, lurl
from tg.i18n import ugettext as _
from tg.exceptions import HTTPFound
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController

from androidbazaar import model
from androidbazaar.controllers.secure import SecureController
from androidbazaar.controllers.application import ApplicationController
from androidbazaar.model import DBSession, User, Application, Release


from androidbazaar.lib.base import BaseController
from androidbazaar.controllers.developer import DeveloperController
from androidbazaar.controllers.admin import AndroidBazaarAdminController
from androidbazaar.controllers.error import ErrorController
from androidbazaar.controllers.api import user, application, thumbnail, purchase, release, withdraw, report

__all__ = ['RootController']


class APIController(BaseController):
    users = user.UserController()
    applications = application.ApplicationController()
    thumbnails = thumbnail.ThumbnailController()
    purchases = purchase.PurchaseController()
    releases = release.ReleaseController()
    withdraws = withdraw.WithdrawController()
    reports = report.ReportController()


class RootController(BaseController):
    secc = SecureController()
    admin = AdminController(model, DBSession, config_type=TGAdminConfig)
    area51 = AndroidBazaarAdminController()
    developers = DeveloperController()
    applications = ApplicationController()
    api = APIController()

    error = ErrorController()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "androidbazaar"

    @expose('androidbazaar.templates.index')
    def index(self):
        q = DBSession.query(Application).join(Release).filter(Release.status == 'published').all()
        return dict(applications=q)

    @expose('androidbazaar.templates.search')
    def search(self, query):
        q = DBSession.query(Application).join(Release).filter(Release.status == 'published')
        if query:
            q = q.filter(Application.label.contains(query))
        q = q.all()
        return dict(applications=q, query=query)

    @expose('androidbazaar.templates.category')
    def category(self, uid, title):
        applications = DBSession.\
            query(Application).\
            join(Release).\
            filter(Release.status == 'published').\
            filter(Application.category_id == uid).\
            all()
        return dict(applications=applications)

    @expose('androidbazaar.templates.developer')
    def developer(self, uid, user_name):
        u = User.one_or_none(uid)
        if not u:
            abort(404)
        applications = DBSession.\
            query(Application).\
            filter(Application.user_id == uid).\
            all()
        return dict(name=u.user_name, applications=applications)

    @expose('androidbazaar.templates.login')
    def login(self, came_from=lurl('/')):
        return dict(came_from=came_from)

    @expose('androidbazaar.templates.register')
    def register(self):
        return dict()

    @expose()
    def post_login(self):
        if not request.identity:
            return 'False'

        if not User.current().is_dev:
            redirect('/logout_handler')

        return 'True'

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('We hope to see you soon!'))
        return HTTPFound(location=came_from)


