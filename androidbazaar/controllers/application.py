from tg import expose, abort

from androidbazaar.lib.base import BaseController
from androidbazaar.model import Application


class ApplicationController(BaseController):

    @expose('androidbazaar.templates.application')
    def details(self, app_uid, package_name, app_title):
        application = Application.one_or_none(app_uid)
        if not application or application.latest_release().status != 'published':
            abort(404)

        return dict(application=application, similar=application.similar_apps())
