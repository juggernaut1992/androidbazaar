from threading import Thread

import transaction
import cherrypy


from androidbazaar import model
from androidbazaar.tests import TestController
from androidbazaar.tests.mock import mock
from androidbazaar.tests.functional import PATH_TO_APK, PATH_TO_NON_APK


class TestApplication(TestController):

    def setUp(self):
        super(TestApplication, self).setUp()
        model.DBSession.add(model.Category(name='test'))
        model.DBSession.add(model.Category(name='test2'))
        transaction.commit()
        payload = {
            'user_name': 'user',
            'password': 'password',
            'email_address': 'email@address.com',
            'phone_number': '00000000000'
        }
        self.app.post_json('/api/users/', params=payload)
        self.environ = {'REMOTE_USER': 'manager'}
        self.app.get(
            '/login_handler?login={}&password={}'.format(payload['user_name'], payload['password']),
        )
        mock_server = Thread(target=self._http_server, daemon=True)
        mock_server.start()

    def _http_server(self):
        mock()

    def test_application(self):
        payload = {'cat_id': 1}
        with open(PATH_TO_APK, 'rb') as apk:
            files = [('apk', 'apk', apk.read())]
            post_resp = self.app.post(
                '/api/applications',
                upload_files=files,
                params=payload,
                extra_environ=self.environ
            ).json

        # Get the app just posted
        app = self.app.get(
            '/api/applications/{}'.format(post_resp.get('app_id')),
            extra_environ=self.environ
        ).json.get('application')
        assert app.get('price') == 0
        assert app.get('category_id') == 1
        assert app.get('user_id') == 1
        assert app.get('package_name') == 'net.nextvpn'
        assert app.get('label') == 'Next VPN'
        assert app.get('en_description') is None
        assert app.get('fa_description') is None

        # Put on the app
        payload = {
            'price': 1000,
            'cat_id': 2,
            'label': 'new label',
            'en_description': 'new en description',
            'fa_description': 'new fa description'
        }

        resp = self.app.put(
            '/api/applications/{}'.format(post_resp.get('app_id')),
            params=payload,
            extra_environ=self.environ
        ).json

        # Get the app just put
        app = self.app.get(
            '/api/applications/{}'.format(resp.get('app_id')),
            extra_environ=self.environ
        ).json.get('application')
        assert app.get('price') == payload.get('price')
        assert app.get('category_id') == payload.get('cat_id')
        assert app.get('user_id') == 1
        assert app.get('package_name') == 'net.nextvpn'
        assert app.get('label') == payload.get('label')
        assert app.get('en_description') == payload.get('en_description')
        assert app.get('fa_description') == payload.get('fa_description')

        # TODO: try to change status, package_name

        with open(PATH_TO_NON_APK, 'rb') as non_apk:
            files = [('apk', 'apk', non_apk.read())]
            self.app.post(
                '/api/applications',
                upload_files=files,
                params=payload,
                extra_environ=self.environ,
                status=400
            )

    def tearDown(self):
        cherrypy.engine.exit()
        super(TestApplication, self).tearDown()
