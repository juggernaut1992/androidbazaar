from androidbazaar.tests import TestController


class TestUser(TestController):

    def test_user(self):
        """Registering accounts and logging in"""

        payload = {
            'user_name': 'user',
            'password': 'password',
            'email_address': 'email@address.com',
            'phone_number': '00000000000',
            'is_dev': True
        }
        resp = self.app.post_json('/api/users/', params=payload).json

        get = self.app.get('/api/users/{}'.format(resp.get('user_id'))).json

        payload = {
            'user_name': payload.get('user_name'),
            'password': payload.get('password')
        }
        session = self.app.post_json('/api/users/request_session', params=payload).json.get('session')
        assert len(session) == 32

        assert get['user'].get('user_name') == payload.get('user_name')

        self.app.get('/api/users/123213', status=404)

        invalid_email = {
            'username': 'user',
            'password': 'password',
            'email_address': 'email.address.com',
            'phone_number': '00000000000'
        }
        self.app.post_json('/api/users/', params=invalid_email, status=400)

        invalid_phone_number = {
            'username': 'user',
            'password': 'password',
            'email_address': 'email.address.com',
            'phone_number': '0'
        }
        self.app.post_json('/api/users/', params=invalid_phone_number, status=400)
