# -*- coding: utf-8 -*-
"""Functional test suite for the controllers of the application."""
from os import path


PATH_TO_APK = path.abspath(path.join(path.dirname(__file__), '..', 'stuff', 'imei.apk'))
PATH_TO_NON_APK = path.abspath(path.join(path.dirname(__file__), '..', 'stuff', 'weird.jpg'))
PATH_TO_DEBUG_APK = path.abspath(path.join(path.dirname(__file__), '..', 'stuff', 'imei-debug.apk'))
