from threading import Thread

import cherrypy
import transaction

from androidbazaar import model
from androidbazaar.tests import TestController
from androidbazaar.tests.mock import mock
from androidbazaar.tests.functional import PATH_TO_APK


class TestPurchase(TestController):
    def setUp(self):
        super(TestPurchase, self).setUp()
        model.DBSession.add(model.Category(name='test'))
        transaction.commit()
        payload = {
            'user_name': 'user',
            'password': 'password',
            'email_address': 'email@address.com',
            'phone_number': '00000000000'
        }
        self.app.post_json('/api/users/', params=payload)
        self.environ = {'REMOTE_USER': 'manager'}
        # self.app.get(
        #     '/login_handler?login={}&password={}'.format(payload['user_name'], payload['password']),
        # )
        """Request session"""
        resp = self.app.post_json(
            '/api/users/request_session',
            params={'user_name': payload.get('user_name'), 'password': payload.get('password')}
        ).json
        self.session = resp.get('session')
        self.user = resp.get('user')

        mock_server = Thread(target=self._http_server, daemon=True)
        mock_server.start()

        payload = {'cat_id': 1}
        with open(PATH_TO_APK, 'rb') as apk:
            files = [('apk', 'apk', apk.read())]
            self.post_resp = self.app.post(
                '/api/applications',
                upload_files=files,
                params=payload,
                extra_environ=self.environ
            ).json

    def _http_server(self):
        mock()

    def test_purchase(self):
        headers = {
            'session': self.session,
            'user': str(self.user.get('uid'))
        }
        self.app.post('/api/purchases/{}'.format(self.post_resp.get('app_id')), headers=headers)

        # Purchase twice
        self.app.post('/api/purchases/{}'.format(self.post_resp.get('app_id')), headers=headers, status=409)

    def tearDown(self):
        cherrypy.engine.exit()
        super(TestPurchase, self).tearDown()
