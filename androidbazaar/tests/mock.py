import cherrypy
from tg import config


class MockController(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def contents(self, *args, **kwargs):
        return {'content_uid': 1}


def mock():
    global_conf = {
        'server.socket_host': config.get('buzzle_host'),
        'server.socket_port': int(config.get('buzzle_port')),
    }
    cherrypy.config.update(global_conf)
    cherrypy.tree = cherrypy._cptree.Tree()
    cherrypy.tree.mount(MockController(), '/api/')

    cherrypy.engine.signals.subscribe()
    cherrypy.engine.start()
    cherrypy.engine.block()
