from json.decoder import JSONDecodeError

from tg import request, abort
from sqlalchemy.ext.associationproxy import ASSOCIATION_PROXY
from sqlalchemy.inspection import inspect

from androidbazaar.model import DBSession


PAGINATION_SIZE = 15


class ConstructorMixin:
    @classmethod
    def one_or_none(cls, uid):
        return DBSession.query(cls).filter(cls.uid == uid).one_or_none()

    def delete(self):
        DBSession.remove(self)
        DBSession.flush()

    @classmethod
    def all(cls):
        return DBSession.query(cls).all()

    @classmethod
    def fetch_by_offset(cls, me=None, page_number=0):
        query = DBSession.query(cls)
        if me:
            query = query.filter(cls.user_id == me.user_id)
        """Requires repoze.who login"""
        return query. \
            order_by(cls.uid.desc()). \
            limit(PAGINATION_SIZE). \
            offset(page_number * PAGINATION_SIZE)

    @classmethod
    def update_from_request(cls, instance=None, kwargs=None):

        if not instance:
            instance = cls()

        data = cls._extract_json_from_request() or kwargs
        if not data:
            abort(400, detail='No data in request!', passthrough='json')

        for c in cls.iter_columns():
            value = data.get(c.expression.description)
            if hasattr(c, 'validate') and c.validate(value):
                instance.__setattr__(c.name, value)

        DBSession.add(instance)
        DBSession.flush()

        return instance

    @classmethod
    def _extract_json_from_request(cls):
        try:
            if request.content_type != 'application/json':
                return
            return request.json
        except JSONDecodeError:
            return

    @classmethod
    def as_json(cls):
        raise NotImplementedError

    @classmethod
    def iter_columns(cls):
        mapper = inspect(cls)
        for k, c in mapper.all_orm_descriptors.items():

            if k == '__mapper__':
                continue

            if c.extension_type == ASSOCIATION_PROXY:
                continue

            yield getattr(cls, k)
