from tg import config, request, abort
from redis import StrictRedis

from androidbazaar.model import User


def authorize():
    def wrapper(f):
        def wrapped(*a, **k):
            user_id = int(request.environ.get('HTTP_USER'))
            session_id = request.environ.get('HTTP_SESSION')
            if not user_id or not session_id:
                abort(401, passthrough='json')

            conn = StrictRedis(db=config.get('redis_session_db'))
            stored_session = conn.get(user_id)
            if not stored_session:
                abort(401, passthrough='json')
            if stored_session.decode() != session_id:
                abort(401, passthrough='json')

            k['user'] = User.one_or_none(user_id)
            return f(*a, **k)
        return wrapped
    return wrapper
