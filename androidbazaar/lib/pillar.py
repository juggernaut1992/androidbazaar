import re

from sqlalchemy import Column
from tg import abort


class Pillar(Column):

    def __init__(self, *args, modifiable=None, pattern=None, min_length=None, max_length=None, **kwargs):
        self.info = dict()
        self.info['pattern'] = pattern
        self.info['modifiable'] = modifiable
        self.info['min_length'] = min_length
        self.info['max_length'] = max_length

        super(Pillar, self).__init__(*args, **kwargs)

    def validate(self, value):
        if self.info.get('modifiable') is not True:
            return False

        if not value:
            abort(400, detail='No value provided for column {}'.format(self.name), passthrough='json')

        _pattern = self.info.get('pattern')
        if _pattern:
            matches = re.match(_pattern, value)
            if not matches:
                abort(400, detail='Invalid value: {} for column {}'.format(value, self.name), passthrough='json')

        min_length = self.info.get('min_length')
        if min_length and len(value) < min_length:
            abort(
                400,
                detail='{} to short, must be at least {} characters'.format(value, min_length),
                passthrough='json'
            )

        max_length = self.info.get('max_length')
        if max_length and len(value) > max_length:
            abort(400, detail='{} to long, must be at most {} characters'.format(value, max_length), passthrough='json')

        return True
