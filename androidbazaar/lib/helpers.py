# -*- coding: utf-8 -*-
"""Template Helpers used in androidbazaar."""
import logging
import random
import string
import re
from markupsafe import Markup
from datetime import datetime

from tg import config

log = logging.getLogger(__name__)

EMAIL_PATTERN = """(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\
    x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:
    [a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9]
    [0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-
    \x7f])+)\])"""

CHUNK_SIZE = 2048
AAPT_TIMEOUT = 3


def current_year():
    now = datetime.now()
    return now.strftime('%Y')


def icon(icon_name):
    return Markup('<i class="glyphicon glyphicon-%s"></i>' % icon_name)


def global_template_variables():
    return dict(
        base_url=config.get('base_url'),
        buzzle_url='{}:{}'.format(
            config.get('buzzle_host'),
            config.get('buzzle_port')
        )
    )


def random_string(n):
    return ''.join(
        random.SystemRandom().choice(
            string.ascii_uppercase + string.ascii_lowercase
        ) for _ in range(n)
    )


def human_readable_size(size):
    suffixes = ['بایت', 'کیلوبایت', 'مگابایت']
    suffix_index = 0
    while size > 1024 and suffix_index < 2:
        suffix_index += 1
        size = size / 1024.0
    size = "%.2f" % size
    return "%s %s" % (convert_en_numbers(size), suffixes[suffix_index])


def multiple_replace(mapping, text):
    pattern = "|".join(map(re.escape, mapping.keys()))
    return re.sub(pattern, lambda m: mapping[m.group()], str(text))


def convert_en_numbers(input_str):
    mapping = {
        '0': '۰',
        '1': '۱',
        '2': '۲',
        '3': '۳',
        '4': '۴',
        '5': '۵',
        '6': '۶',
        '7': '۷',
        '8': '۸',
        '9': '۹',
        '.': '.',
    }
    return multiple_replace(mapping, input_str)
