from cgi import FieldStorage

from tg import config
from requests import post
import jwt


class BuzzleClient:
    def __init__(self, filename, file_path):
        self.url = 'http://{}:{}/api/contents'.format(config.get('buzzle_host'), config.get('buzzle_port'))
        self.filename = filename
        self.file = file_path

    def store(self):
        resp = post(self.url, files={"file": (self.filename, open(self.file, 'rb'))}, headers=auth_headers())
        return resp.json().get('content_uid') if resp.status_code == 200 else None


def auth_headers():
    secret_key = config.get('secret_key')
    secret_code = config.get('secret_code')
    return {
        'X-AUTH-TOKEN': jwt.encode({'code': secret_code}, secret_key, algorithm='HS256').decode()
    }
