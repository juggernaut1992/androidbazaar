import subprocess
import re
from os import path, remove
import tempfile
from cgi import FieldStorage

from tg import config, abort

from androidbazaar.lib.helpers import CHUNK_SIZE


class Package:
    name = None
    version = None
    sdkVersion = None
    label = None
    debug = None
    sha1 = None
    owner = None
    validity_date = None
    permissions = []

    @classmethod
    def from_parser(cls, parse_result, debug_result, cert):
        instance = cls()
        try:
            instance.name = re.search("name='(.*)' versionCode", parse_result).group(1)
            instance.version = re.search("versionName='(.*)'", parse_result).group(1)
            instance.sdkVersion = re.search("sdkVersion:'(.*)'", parse_result).group(1)
            instance.label = re.search("label='(.*)' icon", parse_result).group(1)
        except AttributeError:
            abort(400, detail='Broken package!', passthrough='json')
        instance.permissions = re.findall("uses-permission:'(.*)'", parse_result)
        instance.debug = True if 'CN=Android Debug' in debug_result else False

        instance.sha1 = re.search("SHA1: (.*)\n", cert.decode()).group(1).strip()
        instance.validity_date = re.search("Valid from: (.*)\n", cert.decode()).group(1).strip()
        instance.owner = re.search("Owner: (.*)\n", cert.decode()).group(1).strip()

        return instance


class APKParser:
    def __init__(self, apk_file: FieldStorage):
        self.apk = apk_file
        self.temporary_storage = tempfile.mkdtemp()
        self.file_path = path.join(self.temporary_storage, self.apk.filename)
        self._store_temporarily()
        self.aapt = config.get('aapt')
        self.jarsigner = config.get('jarsigner')

    def parse(self):
        parse_command = "{} dump badging {}".format(self.aapt, self.file_path)
        p = subprocess.Popen(parse_command.split(' '), stdout=subprocess.PIPE)
        parse_out, parse_err = p.communicate()

        debug_command = "{} -verify -verbose -certs {}".format(self.jarsigner, self.file_path)
        p = subprocess.Popen(debug_command.split(' '), stdout=subprocess.PIPE)
        debug_out, debug_err = p.communicate()

        certificate_check_command = 'keytool -list -printcert -jarfile {}'.format(self.file_path)
        p = subprocess.Popen(certificate_check_command.split(' '), stdout=subprocess.PIPE)
        cert_out, cert_err = p.communicate()

        package = Package.from_parser(parse_out.decode(), debug_out.decode(), cert_out)
        return package

    def _store_temporarily(self):
        with open(self.file_path, 'wb') as tmp:
            while True:
                chunk = self.apk.file.read(CHUNK_SIZE)
                if not chunk:
                    break
                tmp.write(chunk)

    def cleanup(self):
        remove(self.file_path)
