a = """
        package: name='juggernaut.com.imei' versionCode='101' versionName='1.1'
        sdkVersion:'14'
        targetSdkVersion:'14'
        uses-gl-es:'0x20000'
        uses-permission:'android.permission.WRITE_EXTERNAL_STORAGE'
        uses-permission:'android.permission.INTERNET'
        uses-permission:'android.permission.READ_PHONE_STATE'
        application-label:'IMEI Validator'
        application-icon-160:'res/drawable/icon.png'
        application-icon-240:'res/drawable/icon.png'
        application-icon-320:'res/drawable/icon.png'
        application-icon-480:'res/drawable/icon.png'
        application: label='IMEI Validator' icon='res/drawable/icon.png'
        application-debuggable
        launchable-activity: name='org.kivy.android.PythonActivity'  label='IMEI Validator' icon=''
        uses-permission:'android.permission.READ_EXTERNAL_STORAGE'
        uses-implied-permission:'android.permission.READ_EXTERNAL_STORAGE','requested WRITE_EXTERNAL_STORAGE'
        uses-feature:'android.hardware.touchscreen'
        uses-implied-feature:'android.hardware.touchscreen','assumed you require a touch screen unless explicitly made optional'
        uses-feature:'android.hardware.screen.portrait'
        uses-implied-feature:'android.hardware.screen.portrait','one or more activities have specified a portrait orientation'
        main
        supports-screens: 'small' 'normal' 'large' 'xlarge'
        supports-any-density: 'true'
        locales: '--_--'
        densities: '160' '240' '320' '480'
        native-code: 'armeabi'
    """
import re

all = re.findall("uses-permission:'(.*)'", a)
print()

