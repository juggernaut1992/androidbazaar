from threading import Thread
import time
from os import fork

def worker():
    try:
        time.sleep(15)
    finally:
        time.sleep(3)
        print('finally is reached')


t = Thread(target=worker())
t.start()
