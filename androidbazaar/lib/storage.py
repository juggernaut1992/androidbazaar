import uuid
from os import path, mkdir

from tg import config
from tg.util.files import safe_filename


CHUNK_SIZE = 4096


class Attachment:
    filename = None
    folder_name = None
    max_size = None
    min_size = None
    content_type = None

    @classmethod
    def from_field_storage(cls, data):
        instance = cls()
        folder_name = cls.generate_uuid()
        original_filename = safe_filename(data.filename)
        destination = cls._specify_path(folder_name, original_filename)

        with open(destination, mode='wb') as dest:
            while True:
                chunk = data.file.read(CHUNK_SIZE)
                if not chunk:
                    break
                dest.write(chunk)

        instance.folder_name = folder_name
        instance.filename = original_filename

        return instance

    @staticmethod
    def _specify_path(folder_name, original_filename):
        storage_path = path.join(config['paths']['static_files'], 'storage')
        if not path.exists(storage_path):
            mkdir(storage_path)

        destination_folder = path.join(storage_path, folder_name)
        mkdir(destination_folder)
        return path.join(destination_folder, original_filename)

    @staticmethod
    def generate_uuid():
        return str(uuid.uuid1())


class ThumbnailAttachment(Attachment):
    max_size = 100 * 1024
    min_size = 1
    content_type = 'image/jpeg'


class AppLogoAttachment(Attachment):
    max_size = 50 * 1024
    min_size = 1
    content_type = 'image/jpeg'


class IDCardAttachment(Attachment):
    pass
