 $(document).ready(function () {
 // toat popup js
    $.toast({
        heading: 'به پنل توسعه دهندگان خوش آمدید'
        , text: 'در اینجا می توانید به تمامی موارد مدیریتی اپلیکیشن های خود دسترسی داشته باشید.'
        , position: 'top-right'
        , loaderBg: '#fff'
        , icon: 'success'
        , hideAfter: 3500
        , stack: 6
    });
// Dashboard 1 Morris-chart
Morris.Area({
    element: 'morris-area-chart2'
    , data: [{
            period: '1391'
            , SiteA: 50
            , SiteB: 0
        , }, {
            period: '1392'
            , SiteA: 160
            , SiteB: 100
        , }, {
            period: '1393'
            , SiteA: 110
            , SiteB: 60
        , }, {
            period: '1394'
            , SiteA: 60
            , SiteB: 200
        , }, {
            period: '1395'
            , SiteA: 130
            , SiteB: 150
        , }, {
            period: '1396'
            , SiteA: 200
            , SiteB: 90
        , }
        , {
            period: '1397'
            , SiteA: 100
            , SiteB: 150
        , }]
    , xkey: 'period'
    , ykeys: ['SiteA', 'SiteB']
    , labels: ['Site A', 'Site B']
    , pointSize: 0
    , fillOpacity: 0.1
    , pointStrokeColors: ['#79e580', '#2cabe3']
    , behaveLikeLine: true
    , gridLineColor: '#ffffff'
    , lineWidth: 2
    , smooth: true
    , hideHover: 'auto'
    , lineColors: ['#79e580', '#2cabe3']
    , resize: true
})
 });
