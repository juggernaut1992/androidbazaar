# -*- coding: utf-8 -*-

#  Quickstarted Options:
#
#  sqlalchemy: True
#  auth:       sqlalchemy
#  mako:       True
#
#

# This is just a work-around for a Python2.7 issue causing
# interpreter crash at exit when trying to log an info message.
import os
import re
import sys
py_version = sys.version_info[:2]

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages


with open(
        os.path.join(
            os.path.dirname(__file__),
            'androidbazaar', '__init__.py')) as v_file:
    VERSION = re.compile(
        r".*__version__ = '(.*?)'",
        re.S).match(v_file.read()).group(1)

testpkgs = [
    'WebTest >= 1.2.3',
    'nose',
    'coverage',
    'gearbox',
    'cherrypy',
    'routes'
]

install_requires = [
    "TurboGears2 >= 2.3.11",
    "Beaker >= 1.8.0",
    "Kajiki >= 0.6.3",
    "Mako",
    "zope.sqlalchemy >= 0.4",
    "sqlalchemy",
    "alembic",
    "repoze.who",
    "tw2.forms",
    "tgext.admin >= 0.6.1",
    "WebHelpers2",
    "gunicorn",
    "psycopg2",
    "requests",
    "pyjwt",
    "khayyam",
    "redis",
    "packaging"
]

if py_version != (3, 2):
    # Babel not available on 3.2
    install_requires.append("Babel")

setup(
    name='androidbazaar',
    version=VERSION,
    description='',
    author='',
    author_email='',
    url='',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=install_requires,
    include_package_data=True,
    test_suite='nose.collector',
    tests_require=testpkgs,
    package_data={'androidbazaar': [
        'i18n/*/LC_MESSAGES/*.mo',
        'templates/*/*',
        'public/*/*'
    ]},
    message_extractors={'androidbazaar': [
        ('**.py', 'python', None),
        ('templates/**.mak', 'mako', None),
        ('templates/**.xhtml', 'kajiki', {'strip_text': False, 'extract_python': True}),
        ('templates/**.mako', 'mako', None),
    #        ('public/**', 'ignore', None)
    ]},
    entry_points={
        'paste.app_factory': [
            'main = androidbazaar.config.middleware:make_app'
        ],
        'gearbox.plugins': [
            'turbogears-devtools = tg.devtools'
        ]
    },
    zip_safe=False
)
print(VERSION)