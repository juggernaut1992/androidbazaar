$TTL    604800
@       IN      SOA     androidbazzar.com. root.androidbazzar.com. (
                              5         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
        IN      A       130.185.76.183
;
@       IN      NS      ns1.androidbazzar.com.
@       IN      A       130.185.76.183
@       IN      AAAA    ::1
ns1     IN      A       130.185.76.183
www     IN      A       130.185.76.183
blog    IN      A       136.243.65.215